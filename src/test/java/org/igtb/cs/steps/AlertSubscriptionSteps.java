package org.igtb.cs.steps;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;   
import org.apache.poi.util.SystemOutLogger;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.joda.time.LocalTime;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class AlertSubscriptionSteps extends BaseFunctions{

	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> alertExcelData = null;
	private String sAlertSubscriptionName = null; 
	private String sAlertSubscriptionCode = null; 
	private String sAlertSubscriptionDescription = null;
	private Map<String,String> temp =null;
	
	public AlertSubscriptionSteps(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Alert Subscription");
		alertExcelData = excel.getExcelDataIntoHashMap();
	}
	
	@When("^I add New Alert Subscription for \"(.*)\"$")
	public void I_add_Alert_Subscription(String sAlertSubscriptionName) throws Exception{
	
		this.sAlertSubscriptionName = sAlertSubscriptionName;
		temp = CSAutomationHooks.alertJSONData.get(this.sAlertSubscriptionName);
		Map<String,String> alertContent = alertExcelData.get(sAlertSubscriptionName);
		
		sAlertSubscriptionCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		sAlertSubscriptionDescription = sAlertSubscriptionName+sAlertSubscriptionCode;
		
		temp.put("alertSubscriptionCode", sAlertSubscriptionCode);
		temp.put("alertSubscriptionDescription", sAlertSubscriptionDescription);
		
		//Enter Alert Subscription
		clickElement("Add new Alert Subscription");
        enterText("Alert Subscription Code",sAlertSubscriptionCode);
       
	    //Enter Alert Subscription Description
        enterText("Alert Subscription Description",sAlertSubscriptionDescription);
        Thread.sleep(4000);
		
		//Select Product
        selectElementFromList("SubsProduct",alertContent.get("Product"));
		Thread.sleep(4000);
		
		//Select Event
        enterText("SubsEvent", alertContent.get("Event"));
        Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		//Select Message Template
		enterText("Alert Message Template",temp.get("alertMessageTemplateCode"));
		Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		//Select Recipient Type
		selectElementFromList("Recipient_Type",alertContent.get("RecipientType"));
		Thread.sleep(4000);
	
		//Select Recipient Class
		enterText("Recipient_Class", alertContent.get("RecipientClass"));
        Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		if(temp.get("SMS").equalsIgnoreCase("Yes")){
			
			clickElement("SMS");
			
		}
		
		if(temp.get("Email").equalsIgnoreCase("Yes")){
			
			clickElement("Email");
			
		}
		if(temp.get("Fax").equalsIgnoreCase("Yes")){
			
			clickElement("Fax");
			
		}
		if(temp.get("Print").equalsIgnoreCase("Yes")){
			
			clickElement("Print");
			
		}
		
		
		clickElement("Submit");
	}
	
	
	//Hold Time
	@When("^I add New Alert Subscription for \"([^\"]*)\" with hold time$")
	public void i_add_New_Alert_Subscription_for_with_hold_time(String sAlertSubscriptionName) throws Exception {
		this.sAlertSubscriptionName = sAlertSubscriptionName;
		temp = CSAutomationHooks.alertJSONData.get(this.sAlertSubscriptionName);
		Map<String,String> alertContent = alertExcelData.get(sAlertSubscriptionName);
		//TimeSetup
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		ZonedDateTime  now = ZonedDateTime.now(ZoneId.of("Asia/Bangkok")); 
		now = now.minusMinutes(15);
		
		sAlertSubscriptionCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		sAlertSubscriptionDescription = sAlertSubscriptionName+sAlertSubscriptionCode;
		
		temp.put("alertSubscriptionCode", sAlertSubscriptionCode);
		temp.put("alertSubscriptionDescription", sAlertSubscriptionDescription);
		
		//Enter Alert Subscription
		clickElement("Add new Alert Subscription");
        enterText("Alert Subscription Code",sAlertSubscriptionCode);
       
	    //Enter Alert Subscription Description
        enterText("Alert Subscription Description",sAlertSubscriptionDescription);
        Thread.sleep(4000);
		
		//Select Product
        selectElementFromList("SubsProduct",alertContent.get("Product"));
		Thread.sleep(4000);
		
		//Select Event
        enterText("SubsEvent", alertContent.get("Event"));
        Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		//Select Message Template
		enterText("Alert Message Template",temp.get("alertMessageTemplateCode"));
		Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		//Select Recipient Type
		selectElementFromList("Recipient_Type",alertContent.get("RecipientType"));
		Thread.sleep(4000);
	
		//Select Recipient Class
		enterText("Recipient_Class", alertContent.get("RecipientClass"));
        Thread.sleep(4000);
		clickElement("NG2-AUTO-COMPLETE-SUBS");
		Thread.sleep(4000);
		
		if(temp.get("SMS").equalsIgnoreCase("Yes")){
			
			clickElement("SMS");
			String strTime = dtf.format(now);
			enterText("Start Time",strTime );
			now = now.plusMinutes(30);
			strTime = dtf.format(now);
			enterText("End Time", strTime);
			 
		}
		
		if(temp.get("Email").equalsIgnoreCase("Yes")){
			
			clickElement("Email");
			
		}
		if(temp.get("Fax").equalsIgnoreCase("Yes")){
			
			clickElement("Fax");
			
		}
		if(temp.get("Print").equalsIgnoreCase("Yes")){
			
			clickElement("Print");
			
		}
		
		
		clickElement("Submit");
	    
	}


	

	@Then("^I approve Alert Subscription$")
	public void I_approve_Alert_Subscription() throws Exception{
		clickElement("Alert Subscription Code Filter");
		enterText("Alert Subscription Filter TextBox",sAlertSubscriptionCode+"\n");
//		Log.info("Before Approve = " + getText("Alert Message Template Status",sAlertSubscriptionCode));
		clickElement("Alert Subscription CheckBox",sAlertSubscriptionCode);
		clickElement("APPROVE");
		enterText("Remarks","Approved"+sAlertSubscriptionCode);
		clickElement("APPROVE Remarks");		
//		Log.info("After Approve = " + getText("Alert Message Template Status",sAlertSubscriptionCode));
	}
	
	@And("^I verify that Alert Subscription is \"(.*)\" successfully$")
	public void I_verify_that_Message_Template_status(String sStatus) throws Exception{
		clickElement("Alert Subscription Code Filter");
		enterText("Alert Subscription Filter TextBox",sAlertSubscriptionCode+"\n");	
		Assert.assertTrue("Alert Subscription Status "+sStatus+" is not found for "+sAlertSubscriptionCode, getText("Alert Subscription Status",sAlertSubscriptionCode).equals(sStatus));
		temp.put("alertSubscriptionStatus", sStatus);
		CSAutomationHooks.alertJSONData.put(sAlertSubscriptionName, temp);
		
		
	}
	
	
	
	


}

package org.igtb.cs.steps;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class BillerCategory extends BaseFunctions {


		TestContext testContext;
		private ExcelUtils excel = new ExcelUtils();
		private Map<String,Map<String,String>> BillCategoryExcelData = null;
		private String sBillCategory = null; 
		private String sBillCategoryCode = null;
		private String sBillCategoryName = null;
		private Map<String,String> temp =null;
		 
		
		public BillerCategory(TestContext context) throws Exception{
			super(context);
			testContext = context;
			excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Category");
			BillCategoryExcelData = excel.getExcelDataIntoHashMap();
		}
		
		@When("^I add New Bill Category \"([^\"]*)\"$")
		public void i_add_New_Bill_Category(String sBillCategory) throws Exception {
		    // Write code here that turns the phrase above into concrete actions
			
			this.sBillCategory = sBillCategory;
			temp = CSAutomationHooks.billpayJSONData.get(this.sBillCategory);
			Map<String,String> billCategoryContent = BillCategoryExcelData.get(sBillCategory);
			sBillCategoryCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
			sBillCategoryName = sBillCategory+sBillCategoryCode;
			temp.put("billercategorycode", sBillCategoryCode);
			temp.put("billercategoryname", sBillCategoryName);
			String sServiceTypeCode=billCategoryContent.get("Service Type Code");
			String sServiceTypeName=billCategoryContent.get("Service Type Name");
			clickElement("Add New Biller Category");
			enterText("Biller Category Code",sBillCategoryCode);
			enterText("Biller Category Name",sBillCategoryName);
			temp.put("bank_entity",billCategoryContent.get("Bank-Entity").toString());
			clickElement("EPayment",billCategoryContent.get("E-Payment")); 
					
		    
			String[] ServiceTypeCode=sServiceTypeCode.split(",");
			String[] ServiceTypeName=sServiceTypeName.split(",");
			
		      int i=0,j=1;
		      for(String STCode:ServiceTypeCode)
		      {
		    	  clickElement("Add Service Type Details");
		    	  enterText("Service Type Code",String.valueOf(j),STCode);
		    	  enterText("Service Type Name",String.valueOf(j),ServiceTypeName[i]);
		    	  Log.info(STCode);
		    	  Log.info(ServiceTypeName[i]);
		    	  temp.put("ServiceType"+i,STCode+" - "+ServiceTypeName[i]);
		    	  temp.put("ServiceTypeCode"+i, STCode);
		    	  temp.put("ServiceTypeName"+i, ServiceTypeName[i]);
		    	  i++;
		    	  j++;
		    	  
		      }
		  	Thread.sleep(2000);
			
			
			if(isElementEnabled("Submit")){
				clickElement("Submit");
			}else{
				Assert.assertTrue("Error while adding Biller Category",false);
			}
			Thread.sleep(3000);
		//	temp.put("billercategorycode", sBillCategoryCode);
			//temp.put("billercategoryname", sBillCategoryName);
		//	
		}
		
		@When("^I verify that Bill Category is \"([^\"]*)\" successfully$")
		public void i_verify_that_Bill_Category_is_successfully(String sStatus) throws Exception {
		    // Write code here that turns the phrase above into concrete actions
			Thread.sleep(2000);
			clickElement("CLEARFILTER");
			Thread.sleep(2000);
			clickElement("Bill Category Code Filter");
			Thread.sleep(2000);
			enterText("Bill Category Code Filter TextBox",sBillCategoryCode+"\n");	
			Thread.sleep(2000);
			Assert.assertTrue("Bill Category Code Status"+sStatus+" is not found for "+sBillCategoryCode, getText("Bill Category Code Status",sBillCategoryCode).equals(sStatus));
		    temp.put("BillCategoryStatus", sStatus);
		}
		
		@Then("^I approve Bill Category$")
		public void i_approve_Bill_Category() throws Exception {
		    // Write code here that turns the phrase above into concrete actions
			Thread.sleep(2000);
			clickElement("Bill Category Code Filter");
			Thread.sleep(2000);
			enterText("Bill Category Code Filter TextBox",sBillCategoryCode+"\n");
			Thread.sleep(2000);
			clickElement("Bill Category Code CheckBox",sBillCategoryCode);
			clickElement("Approve1");
			enterText("Remarks","Approved"+sBillCategoryCode);
			clickElement("APPROVE Remarks");
		}

}

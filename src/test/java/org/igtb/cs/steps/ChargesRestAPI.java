package org.igtb.cs.steps;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.DatabaseConnection;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.remote.server.handler.SendKeys;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ChargesRestAPI extends BaseFunctions{

	TestContext testContext;
	private Response response;
	private RequestSpecification request;
	private String sChargeTriggerName = null; 
	private String sChargesPackageCode = null; 
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> ChargesExcelData = null;
	private Map<String,String> temp =null;
	private Map<String,String> chargeContent = null;
	private String txnRef = null;
	//private String sAlertMonitoring = null;
	private String spackageName = null;
	private String entityDate = null;
	
	public ChargesRestAPI(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Charge Trigger");
		ChargesExcelData = excel.getExcelDataIntoHashMap();		
	}

	
	
	@Then("^I verify the return charge event status code is (\\d+)$")
	public void i_verify_the_return_charge_event_status_code_is(int statusCode) {
	    // Write code here that turns the phrase above into concrete actions
		response.then().statusCode(statusCode);
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_TXN, txnRef);
		Assert.assertTrue("Charge Processing Failed for "+txnRef,DatabaseConnection.getInstance().getResultSetToList().get(0).get("TXN_REF").toString().equals(txnRef));
	}

	@Given("^I link the package with cif_guid for charge \"(.*)\"$")
	public void I_link_the_package_with_cif_guid_for_charge(String sChargeTriggerName){
		this.sChargeTriggerName = sChargeTriggerName;
		temp = CSAutomationHooks.chargeJSONData.get(this.sChargeTriggerName);
		chargeContent = ChargesExcelData.get(sChargeTriggerName);
		Log.info("CHARGE_PACKAGE_CODE = " + temp.get("packagecode"));
		Log.info("CIF_GUID = " + chargeContent.get("CIF_GUID"));
		sChargesPackageCode = temp.get("packagecode");
		Log.info("sChargesPackageCode from JSON= " + sChargesPackageCode);
	
		DatabaseConnection.getInstance().executeUpdateQuery(String.format(DBQueries.CORPORATE_PACKAGE_LINKAGE, sChargesPackageCode, sChargesPackageCode, sChargesPackageCode, sChargesPackageCode, chargeContent.get("CIF_GUID") ));
		
	} 

	@Given("^I trigger the Charge Processing$")
	public void I_trigger_the_charge_processing() throws Exception{
//		String outString = FileUtils.readFileToString(new File(AutomationHooks.RESOURCES_PATH+"/TestData/AlertsRequest.json"), "UTF-8");
		String outString = createChargeRequest();
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT1"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT1"));
	}

	private String createChargeRequest() throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("bankEntityId", FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME());
		if(chargeContent.get("Product").equals("Common Services")){
			data.put("product", "COMMON");
		}else if(chargeContent.get("Product").equals("Payments")){
			data.put("product", "PAYMNT");	
		}
		data.put("eventCode", chargeContent.get("Event"));
		data.put("ppProduct", chargeContent.get("PPProduct"));
		txnRef = KeyGenerator.getrandomAlphanumericString(20);
		data.put("txnRef", txnRef);
		Log.info("Transaction Reference Number is: "+txnRef);
		data.put("cifGuid", chargeContent.get("CIF_GUID"));
		data.put("businessProduct", chargeContent.get("Business_Product"));
		
		List<Map<String,String>> list = new ArrayList<>();
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Event Attributes");
		Map<String, Map<String, String>> EventAttributesExcelData = excel.getExcelDataIntoHashMap();	
		EventAttributesExcelData.forEach((key,value)-> {Log.info("Key : " + key + " Value : " + value);
		Map<String,String> map = new LinkedHashMap<>();
		if(!value.get(this.sChargeTriggerName).equalsIgnoreCase("")){
			String[] strings = value.get(this.sChargeTriggerName).split("=", 2);
			map.put("sequence", key);
			map.put("attributeId", strings[0]);
			if(strings[1].equalsIgnoreCase("CurrentDate")){
				map.put("attributeValue", KeyGenerator.getDate("CurrentDate","yyyy-MM-dd"));
			}else{
				map.put("attributeValue", strings[1]);
			}
		}else{
			return;
		}
		list.add(map);
		});
		data.put("chargeAttributes", list);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/ChargesTrigger/").mkdirs();
		File destFile = new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/ChagesTrigger/"+chargeContent.get("Product")+"_"+chargeContent.get("Event")+"_"+txnRef+".json");
		FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		return outString;
	}
	
	
	
	@Then("^I verify billing status and charge amount total for package \"([^\"]*)\"$")
	public void i_verify_billing_status_and_charge_amount_total_for_package(String spackageName) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	//	Thread.sleep(10000);
	//	DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_TXN, txnRef);
	//	String statuss = DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString();
	//	Log.info(statuss);
	//	Thread.sleep(10000);
		//Assert.assertTrue("Charge Processing Failed for "+txnRef,DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString().equals("COMPUTED"));
		
		
		
		//Log.info("Status is : "+DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString());
	/*	DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_COMP_IMMEDIATE_TXN, txnRef);
		Assert.assertTrue("Charge Processing Failed for "+txnRef,DatabaseConnection.getInstance().getResultSetToList().get(0).get("CHARGE_AMNT_TOTAL").toString().equals(chargeContent.get("Charge_Amount_Total")));
		String amount = DatabaseConnection.getInstance().getResultSetToList().get(0).get("CHARGE_AMNT_TOTAL").toString();
		Log.info(amount);*/
		
		this.spackageName = spackageName;
		Map<String, String> chgPkgContent = ChargesExcelData.get(spackageName);
		String ChgName=chgPkgContent.get("Charge_Name1");
	     String[] ChargeName=ChgName.split(",");
	     
	      for(String CHName:ChargeName)
	     {
	    	  temp	= CSAutomationHooks.chargeJSONData.get(CHName);
	    	  Log.info(temp.get("computationcycle"));
	    	  
	    	  if(temp.get("computationcycle").equals("Immediate"))
	    	  {
	    		  if(temp.get("billingcycle").equals("Immediate")||temp.get("billingcycle").equals("Deferred") )
	    		  {
	    			  Log.info("Verify for computation cycle"+temp.get("computationcycle")+" and billing cycle "+temp.get("billingcycle"));
	    			  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_COMP_IMMEDIATE_TXN, txnRef);
	    			Log.info("Charge Amount = "+DatabaseConnection.getInstance().getResultSetToList().get(0).get("CHARGE_AMNT_TOTAL").toString());
	    			 
	    			  
	    		  }
	    	  }else
	    	  {
	    		  Log.info("Verify for computation cycle"+temp.get("computationcycle")+" and billing cycle "+temp.get("billingcycle"));
	    		  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_COMP_DEFFERED_STAGE, txnRef);
	    	  }
	    }

		
	}
	
	@Then("^I verify billing status of deffered codes for package \"([^\"]*)\"$")
	public void i_verify_billing_status_of_deffered_codes_for_package(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		this.spackageName = spackageName;
		
		Map<String, String> chgPkgContent = ChargesExcelData.get(spackageName);
		String ChgName=chgPkgContent.get("Charge_Name1");
	     String[] ChargeName=ChgName.split(",");
	   /*  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BANK_ENTITY,chgPkgContent.get("BANK_ENTITY"));
			String sdate = DatabaseConnection.getInstance().getResultSetToList().get(0).get("BUSINESS_DATE").toString();
			Log.info(sdate);
			String [] dates = sdate.split(" ");
			Log.info(dates[0]);*/
	      for(String CHName:ChargeName)
	     {
	    	  temp	= CSAutomationHooks.chargeJSONData.get(CHName);
	    	  Log.info(temp.get("computationcycle"));
	    	  
	    	  if(temp.get("computationcycle").equals("Immediate"))
	    	  {
	    		  if(temp.get("billingcycle").equals("Deferred") )
	    		  {
	    			  
	    			  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_BILLING_TXN, chgPkgContent.get("BANK_ENTITY"),chgPkgContent.get("CIF_GUID"),entityDate);  
	    			  
	    		  }
	    	  }else
	    	  {
	    		  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_BILLING_TXN, chgPkgContent.get("BANK_ENTITY"),chgPkgContent.get("CIF_GUID"),entityDate);
	    
	    		  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.CHARGE_COMP_DEFFERED_TXN,temp.get("chargeCode"));
	    		  String amt = DatabaseConnection.getInstance().getResultSetToList().get(0).get("CHARGE_AMNT_TOTAL").toString();
	    		  Log.info("Charge Amount = "+amt);
	    		  
	    	  }
	    }
	}
	
	@Then("^I process EOD batch for \"([^\"]*)\" entity$")
	public void i_process_EOD_batch_for_entity(String sEntity) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BANK_ENTITY,sEntity);
		String sdate = DatabaseConnection.getInstance().getResultSetToList().get(0).get("BUSINESS_DATE").toString();
		Log.info(sdate);
		String [] dates = sdate.split(" ");
		Log.info("Date before EOD batch process " +dates[0]);
		entityDate = dates[0];
		
		String outString = createEODRequest(sEntity,dates[0]);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT2"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT2"));
		Thread.sleep(60000);
		//Verify EOD job status and business date advancement or change
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BANK_ENTITY,sEntity);
		String vdate = DatabaseConnection.getInstance().getResultSetToList().get(0).get("BUSINESS_DATE").toString();
		Log.info(vdate);
		
		Assert.assertNotEquals(sdate, vdate);
		
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.EOD_JOB_STATUS);
		Assert.assertTrue("EOD Processing Failed for Thailand entity ",DatabaseConnection.getInstance().getResultSetToList().get(0).get("RESPONSE_STATUS").toString().equals("SUCCESS"));
		
	}
	
	private String createEODRequest(String uEntity, String udates) throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("id", uEntity);
		data.put("entityType", "B");
		data.put("businessDate", udates);
		data.put("jobId","");
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/ChargesEOD/").mkdirs();
		File destFile = new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/ChagesEOD/"+udates+".json");
		FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		return outString;
	}


}
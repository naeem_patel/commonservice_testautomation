package org.igtb.cs.steps;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BillerServiceConfig extends BaseFunctions{
	
	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> BillerServiceConfigExcelData = null;
	private Map<String,String> temp =null;
	private String sBillerServiceConfig = null; 
	private String BillPresentmentMode;
	private String PayerRegistrationflow;
	private String PayerRegistrationSource;
	
	public BillerServiceConfig(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Category");
		BillerServiceConfigExcelData = excel.getExcelDataIntoHashMap();
	}
	
	@When("^I add New Biller Service Configuration \"([^\"]*)\"$")
	public void i_add_New_Biller_Service_Configuration(String sBillerServiceConfig) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		this.sBillerServiceConfig = sBillerServiceConfig;
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillerServiceConfig);
		Map<String,String> billServiceConfig = BillerServiceConfigExcelData.get(sBillerServiceConfig);
		clickElement("Add New Biller Service Configuration");
		enterText("Biller",temp.get("BillerMaintenanceCode"));
		clickElement("SelectValueBiller");
		enterText("Biller Category",temp.get("billercategorycode"));
		clickElement("SelectValueBillerCategory");
		//enterText("BillerConfigServiceType",temp.get("ServiceTypeCode"));
		Thread.sleep(1000);
		enterText("BillerConfigServiceType",temp.get("ServiceTypeCode2"));
		Thread.sleep(2000);
		clickElement("SelectValueBillerConfigServiceType");
		
		BillPresentmentMode = billServiceConfig.get("Bill Presentment Mode");
		PayerRegistrationflow = billServiceConfig.get("Payer Registration");
		PayerRegistrationSource=billServiceConfig.get("Payer Registration Source");
		
		if(billServiceConfig.get("E-Payment").equals("No"))
		{
			clickElement("Bill Presentment Mode",BillPresentmentMode);
		}
		
		
		if(BillPresentmentMode.equals("Online Fetch") || BillPresentmentMode.equals("Scheduled Push"))
		{
			
			clickElement("Payer Registration",PayerRegistrationflow);
			if(PayerRegistrationflow.equals("Yes"))
				{
				clickElement("Payer Registration Dropdown",PayerRegistrationSource);
				
				if(PayerRegistrationSource.equals("External Biller"))
			clickElement("Registration Data Hand-off",billServiceConfig.get("Registration Data Hand-off"));
				else
					if(PayerRegistrationSource.equals("External Aggregator"))
						clickElement("Payer Registration Confirmation",billServiceConfig.get("Payer Registration Confirmation"));	
					
		
				}
			enterText("Bill Presentment",billServiceConfig.get("Bill Presentment"));	
			clickElement("SelectValueBillPresentment");
			clickElement("Adhoc Payment",billServiceConfig.get("Adhoc Payment"));
			clickElement("Adhoc Prevalidation",billServiceConfig.get("Adhoc Prevalidation"));
			
			if(billServiceConfig.get("Adhoc Prevalidation").equals("Yes"))
			{
				enterText("Adhoc Validation",billServiceConfig.get("Adhoc Validation"));
				clickElement("SelectValueAdhocValidation");	
			}
				if(PayerRegistrationflow.equals("Yes"))
				{
					enterText("IDPayment Registration",billServiceConfig.get("IDPayment Registration"));
					clickElement("SelectValueIDPaymentRegistration");
				}
				clickElement("Type of Bill Generated",billServiceConfig.get("Type of Bill Generated"));	
				clickElement("Payment Restriction",billServiceConfig.get("Payment Restriction"));
		}
		else
			if(BillPresentmentMode.equals("None"))
			{    clickElement("Adhoc Prevalidation",billServiceConfig.get("Adhoc Prevalidation"));
				enterText("Adhoc Validation",billServiceConfig.get("Adhoc Validation"));
				clickElement("SelectValueAdhocValidation");
			}
		
		
		
		
		
		
		
		
		
		
		
		
		
		clickElement("Payment Confirmation Hand-off",billServiceConfig.get("Payment Confirmation Hand-off"));
		
		if(billServiceConfig.get("Is Collection Customer").equals("No"))
		{    
			
			clickElement("Biller Account Type",billServiceConfig.get("Biller Account Type"));
			if(billServiceConfig.get("Biller Account Type").equals("Credit Account"))
			{
			enterText(" Credit Account Number",billServiceConfig.get("Account Number"));
			
			enterText("Account Branch",billServiceConfig.get("Account Branch"));
			clickElement("SelectAccountBranchValue");
			
			enterText("Account Name",billServiceConfig.get("Account Name"));
			enterText("Account Type",billServiceConfig.get("Account Type"));
			clickElement("SelectAccountTypeValue");
			}
			else
				if(billServiceConfig.get("Biller Account Type").equals("Sundry Account"))
				{
					enterText("Sundry Account Number",billServiceConfig.get("Account Number"));
					Thread.sleep(1000);
					clickElement("SelectSundryAccountNumber");
				}
		}
		
		
		
		
	
		
		enterText("Payment Processing",billServiceConfig.get("Payment Processing"));
		clickElement("SelectValuePaymentProcessing");
		
		if(billServiceConfig.get("Bill Parameters").equals("Yes"))
		{
			
			String FieldType=billServiceConfig.get("Field Type");
			String FieldId=billServiceConfig.get("Field Id");
			String FieldName=billServiceConfig.get("Field Name");
			String FieldNameLocal=billServiceConfig.get("Field Name Local");
			String DataType=billServiceConfig.get("Data Type");
			String FieldLenght=billServiceConfig.get("Field Length");
			String InputType=billServiceConfig.get("Input Type");
			String PayerRegistration=billServiceConfig.get("Payer Registration1");
			String FetchData=billServiceConfig.get("Fetch Data");
			String AdhocData=billServiceConfig.get("Adhoc Data");
			
			String[] sFieldType=FieldType.split(",");
			String[] sFieldId=FieldId.split(",");
			String[] sFieldName=FieldName.split(",");
			String[] sFieldNameLocal=FieldNameLocal.split(",");
			String[] sDataType=DataType.split(",");
			String[] sFieldLenght=FieldLenght.split(",");
			String[] sInputType=InputType.split(",");
			String[] sPayerRegistration=PayerRegistration.split(",");
			String[] sFetchData=FetchData.split(",");
			String[] sAdhocData=AdhocData.split(",");
			int i =1, j=2, k=0;
			String td = null;
			for(String STFieldType:sFieldType)
		      {
				clickElement("Add Bill Parameter");
				td = "tr["+i+"]/td["+j+"]";
				selectElementFromList("Field Type", td, STFieldType);
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				clickElement("Field Id", td);
				enterText("Field Id", td, sFieldId[k]);
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				clickElement("Field Name", td);
				enterText("Field Name", td, sFieldName[k]);
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				clickElement("Field Name Local", td);
				enterText("Field Name Local", td, sFieldNameLocal[k]);
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				selectElementFromList("Data Type", td, sDataType[k]);
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				if(sDataType[k].equals("String"))
				{
					clickElement("Field Length", td);
					enterText("Field Length", td, sFieldLenght[k]);
				}
				
				j++;
				td = "tr["+i+"]/td["+j+"]";
				selectElementFromList("Input Type", td, sInputType[k]);
				
				j++;
				j++;
				td = "tr["+i+"]/td["+j+"]";
				if(PayerRegistrationflow.equals("Yes") || PayerRegistrationflow.equals("Pre-Confirmed"))
				{
					
				selectElementFromList("Payer Registration1", td, sPayerRegistration[k]);
				}
				j++;
				td = "tr["+i+"]/td["+j+"]";
				if(BillPresentmentMode.equals("Online Fetch"))
				{ 
				selectElementFromList("Fetch Data", td, sFetchData[k]);
				}
				j++;
				td = "tr["+i+"]/td["+j+"]";
				if(billServiceConfig.get("Adhoc Validation").equals("Yes"))
				{ 	
				
				selectElementFromList("Adhoc Data", td, sAdhocData[k]);
				}
				i++;
				k++;
				j=2;
		      }
			
		}
		
		if(isElementEnabled("Submit1")){
			clickElement("Submit1");
		}else{
			Assert.assertTrue("Error while adding Biller Service Configuration",false);
		}
		Thread.sleep(3000);
				}
		
	
	@When("^I verify that Biller Service Configuration is \"([^\"]*)\" successfully$")
	public void i_verify_that_Biller_Service_Configuration_is_successfully(String sStatus) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		clickElement("CLEARFILTER");
		Thread.sleep(2000);
		clickElement("Bill Id Filter");
		Thread.sleep(2000);
		enterText("Bill Id Filter TextBox",temp.get("BillerMaintenanceCode")+"\n");	
		Thread.sleep(2000);
		clickElement("Biller Category Name Filter");
		Thread.sleep(2000);
		enterText("Biller Category Name Filter TextBox",temp.get("billercategoryname")+"\n");	
		Thread.sleep(2000);
		clickElement("Service Type Name Filter");
		Thread.sleep(2000);
		enterText("Service Type Name Filter TextBox",temp.get("ServiceTypeName2")+"\n");	
		Thread.sleep(2000);
		//Assert.assertTrue("Bill Category Code Status"+sStatus+" is not found for "+sBillCategoryCode, getText("Bill Category Code Status",sBillCategoryCode).equals(sStatus));
	    //temp.put("BillCategoryStatus", sStatus);
	}
	
	@Then("^I approve Biller Service Configuration$")
	public void i_approve_Biller_Service_Configuration() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		clickElement("CLEARFILTER");
		Thread.sleep(2000);
		clickElement("Bill Id Filter");
		Thread.sleep(2000);
		enterText("Bill Id Filter TextBox",temp.get("BillerMaintenanceCode")+"\n");	
		Thread.sleep(2000);
		clickElement("Biller Category Name Filter");
		Thread.sleep(2000);
		enterText("Biller Category Name Filter TextBox",temp.get("billercategoryname")+"\n");	
		Thread.sleep(2000);
		clickElement("Service Type Name Filter");
		Thread.sleep(2000);
		enterText("Service Type Name Filter TextBox",temp.get("ServiceTypeName2")+"\n");	
		Thread.sleep(2000);
		clickElement("Bill Service Config CheckBox",temp.get("BillerMaintenanceCode"));
		clickElement("Approve1");
		enterText("Remarks","Approved"+temp.get("BillerMaintenanceCode"));
		clickElement("APPROVE Remarks");
	}

}

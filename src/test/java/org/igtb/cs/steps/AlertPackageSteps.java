package org.igtb.cs.steps;

import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AlertPackageSteps extends BaseFunctions{

	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> alertExcelData = null;
	private String sAlertPackageName = null; 
	private String sAlertPackageCode = null; 
	private String sAlertPackageDescription = null; 
	private Map<String,String> temp =null;
	 
	
	public AlertPackageSteps(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Alert Package");
		alertExcelData = excel.getExcelDataIntoHashMap();
	}
	
	@When("^I add New Alert Package for \"([^\"]*)\"$")
	public void i_add_New_Alert_Package_for(String sAlertPackageName) throws Exception {
		
		this.sAlertPackageName = sAlertPackageName;
		Map<String,String> alertContent = alertExcelData.get(sAlertPackageName);
		String Subscription = alertContent.get("Alert Subscription");
		String [] SubscriptionList = Subscription.split(",");
		sAlertPackageCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		sAlertPackageDescription = sAlertPackageName+sAlertPackageCode;
		
		clickElement("Add new Alert Package");
		
		enterText("Alert Package Code",sAlertPackageCode);
        enterText("Alert Package Description",sAlertPackageDescription);
        selectElementFromList("Alert Package For",alertContent.get("Alert Package For"));
		Thread.sleep(4000);
		
		for(String temp :SubscriptionList )
		{
			String temp1 = CSAutomationHooks.alertJSONData.get(temp).get("alertSubscriptionDescription");
			selectElementFromList("AlertSubscriptions",temp1);	
		}
		
		
		clickElement("Submit");
	}

	@And("^I verify that Alert Package is \"([^\"]*)\" successfully$")
	public void i_verify_that_Alert_Package_is_successfully(String sStatus) throws Exception {
		Thread.sleep(4000);
		clickElement("Alert Package Code Filter");
		Thread.sleep(4000);
		enterText("Alert Package Filter TextBox",sAlertPackageCode+"\n");	
		Thread.sleep(4000);
		Assert.assertTrue("Alert Package Status "+sStatus+" is not found for "+sAlertPackageCode, getText("Alert Package Status",sAlertPackageCode).equals(sStatus));
		Thread.sleep(4000);
		Map<String,String> alertContent = alertExcelData.get(sAlertPackageName);
		String Subscription = alertContent.get("Alert Subscription");
		String [] SubscriptionList = Subscription.split(",");
		for(String temp1 :SubscriptionList )
		{
			temp = CSAutomationHooks.alertJSONData.get(temp1);
			temp.put("alertPackageCode", sAlertPackageCode);
			temp.put("alertPackageDescription", sAlertPackageDescription);
			CSAutomationHooks.alertJSONData.put(temp1, temp);
		}
	}

	@Then("^I approve Alert Package$")
	public void i_approve_Alert_Package() throws Exception {
		clickElement("Alert Package Code Filter");
		enterText("Alert Package Filter TextBox",sAlertPackageCode+"\n");
//		Log.info("Before Approve = " + getText("Alert Message Template Status",sAlertSubscriptionCode));
		clickElement("Alert Package CheckBox",sAlertPackageCode);
		clickElement("APPROVE");
		enterText("Remarks","Approved"+sAlertPackageCode);
		clickElement("APPROVE Remarks");		
	    
	}

	

}

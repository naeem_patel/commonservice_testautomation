package org.igtb.cs.steps;

import java.util.Map;

import org.apache.poi.util.SystemOutLogger;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;
import org.openqa.selenium.remote.server.handler.GetElementText;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChargeCodeSteps extends BaseFunctions {

	TestContext testContext;
	private ExcelUtils excel1 = new ExcelUtils();
	private ExcelUtils excel2 = new ExcelUtils();
	private Map<String,Map<String,String>> chargeExcelData = null;
	private String sChargeName = null; 
	private String sChargeCode = null; 
	private String sChargeCodeDescription = null;
	private Map<String,String> temp =null;
    private Object[][] data1=null;
    private int row;
	public ChargeCodeSteps(TestContext context) throws Exception {
		super(context);
		testContext = context;
		
		excel1.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls","Charge Code ");
		excel2.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls","Charge Rule");
		chargeExcelData = excel1.getExcelDataIntoHashMap();
		
	
	}

	@When("^I add Charge Code for \"(.*)\"$") 
	public void I_add_Charge_Code(String sChargeName) throws Exception{
		
			
			this.sChargeName = sChargeName;
			temp = CSAutomationHooks.chargeJSONData.get(this.sChargeName);
			Map<String,String> chargeContent = chargeExcelData.get(sChargeName);
			sChargeCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
			sChargeCodeDescription = sChargeName+sChargeCode;
			temp.put("product", chargeContent.get("Product"));
			temp.put("event", chargeContent.get("Charge Event"));
			temp.put("charge-Type", chargeContent.get("Charge Type"));
			temp.put("chg-Currency", chargeContent.get("Charge Currency"));
			temp.put("computationcycle", chargeContent.get("Computation Cycle"));
			temp.put("billingcycle", chargeContent.get("Billing Cycle"));
			temp.put("chargeCode", sChargeCode);
			temp.put("chargeCodeDescription", sChargeCodeDescription);
			
			
			
		clickElement("Add New Charge Code");
        enterText("Charge Code",sChargeCode);
      enterText("Charge Code Description",sChargeCodeDescription);
     
      waitForElementVisibility("Chg_Product");
      selectElementFromList("Chg_Product",chargeContent.get("Product"));
   
      enterText("Charge Event", chargeContent.get("Charge Event"));
      Thread.sleep(4000);
	clickElement("NG2-AUTO-COMPLETE1");
	selectElementFromList("Charge Type",chargeContent.get("Charge Type"));
	enterText("Charge Currency", chargeContent.get("Charge Currency"));
    Thread.sleep(3000);
	clickElement("NG2-AUTO-COMPLETE1");
	enterText("Charge GL", chargeContent.get("Charge  Gl"));
    Thread.sleep(3000);
	clickElement("NG2-AUTO-COMPLETE1");

	
	data1=   excel2.getExcelDataIntoArray();
	 row=data1.length;
	

	//int k=1;
	//	for(int i=1;i<row;i++)
		//{   
			
			//	if(data1[i][0].equals(chargeContent.get("Attribute")) && data1[i][1].equals("Event Attribute"))
			//	{
					
					clickElement("Add new Rule");
			//waitForElementVisibility("Event Attribute",String.valueOf(k));
					
			//	selectElementFromList("Event Attribute",String.valueOf(k),data1[i][2].toString());	
				selectElementFromList("Event Attribute",chargeContent.get("Event Attribute"));
				selectElementFromList("Condition",chargeContent.get("Condition"));
			selectElementFromList("Condition Type",chargeContent.get("Condition Type"));
			
			if(chargeContent.get("Condition Type").equals("STATIC_VALUE"))
			{
				
				enterText("Condition Value Input1",chargeContent.get("Condition Value1"));
			}
			else
			{
				
				
				selectElementFromList("Condition Value Select1",chargeContent.get("Condition Value1"));
			
			}
			
			//}
			
		
	
	Thread.sleep(1000);
	
	
	
	clickElement("Next Step");
	 if(chargeContent.get("Event Type").equals("Transaction"))
	 { selectElementFromList("Computation Cycle",chargeContent.get("Computation Cycle"));
		 
	 }
	
	
  
	
	if((chargeContent.get("Computation Cycle").equals("Deferred") ))
	{
		
		selectElementFromList("Computation Cycle Type",chargeContent.get("Computation Cycle Type"));
		enterText("Computation Period", chargeContent.get("Computation Period"));
	}
	else
	{
		selectElementFromList("Billing Cycle",chargeContent.get("Billing Cycle"));
	}
	
	selectElementFromList("Charge Basis",chargeContent.get("Charge Basis"));
	
	
	
	
	
	if((chargeContent.get("Charge Basis").equals("Slab Amount")) || (chargeContent.get("Charge Basis").equals("Slab Count")))
	{
selectElementFromList("Slab Type",chargeContent.get("Slab Type"));


if((chargeContent.get("Slab Type").equals("Direct Slab")))
{
	enterText("Slab Currency",chargeContent.get("Slab Currency"));
    Thread.sleep(1000);
	clickElement("NG2-AUTO-COMPLETE1");
} 


/*int j=1;
for(int i=1;i<row;i++)
{
	if((data1[i][0].equals(chargeContent.get("Attribute"))) && (data1[i][1].equals("Slab Amount") || data1[i][1].equals("Slab Count")) )
	{
		 if(j!=1)
		 {	 clickElement("Add new Slab");	 }*/
		//enterText("Range To",chargeContent.get("Range To"));
		
		//enterText("Variable Charge Amt",chargeContent.get("Variable Charge(Slab)"));
		if((chargeContent.get("Slab Type").equals("Direct Slab")))
		{  enterText("Fixed Charge Amount",chargeContent.get("Fixed Charges(Slab)"));
			enterText("Min Variable",chargeContent.get("Var Min/Max Amount(Slab)").substring(0,(chargeContent.get("Var Min/Max Amount(Slab)").indexOf('/'))));
			enterText("Max variable",chargeContent.get("Var Min/Max Amount(Slab)").substring(chargeContent.get("Var Min/Max Amount(Slab)").indexOf('/')+1));
		}
		
		
		
	
	//if((row-1)-i>=1)
		//	{
			//	clickElement("Add new Slab");
			//	j++;
			//}


	}

	if(chargeContent.get("Charge Basis").equals("Count") || chargeContent.get("Charge Basis").equals("Slab Count"))
	{
		enterText("Count Attribute", chargeContent.get("Count Attribute"));
	    Thread.sleep(2000);
		clickElement("NG2-AUTO-COMPLETE1");
		
       enterText("Variable Charges Count", chargeContent.get("Var Charges(Header)"));	
	}
	if(chargeContent.get("Charge Basis").equals("Amount"))
	{
			enterText("Variable Charges Amount", chargeContent.get("Var Charges(Header)"));	
	}
	if(!(chargeContent.get("Slab Type").equals("Direct Slab")))
	{
		enterText("Fixed Charges", chargeContent.get("Fixed Charges(header)"));
		enterText("Min Variable Charge Amount", chargeContent.get("Var Min/Max Amount(Header)").substring(0,(chargeContent.get("Var Min/Max Amount(Header)").indexOf('/'))));
		enterText("Max Variable Charge Amount", chargeContent.get("Var Min/Max Amount(Header)").substring(chargeContent.get("Var Min/Max Amount(Header)").indexOf('/')+1));
	}
	
	Thread.sleep(2000);
	
	
	if(isElementEnabled("Submit")){
		clickElement("Submit");
	}else{
		Assert.assertTrue("Error while adding Charge Code",false);
	}
	Thread.sleep(3000);
	}

	
	@Then("^I approve Charge Code$")
	public void I_approve_Charge_Code() throws Exception{
		Thread.sleep(5000);
		clickElement("Charge Code Filter");
		Thread.sleep(5000);
	enterText("Charge Code Filter TextBox",sChargeCode+"\n");
	Thread.sleep(5000);
		//Log.info("Before Approve = " + getText("Charge Code Status",sChargeCode));
	clickElement("Charge Code CheckBox",sChargeCode);
	Thread.sleep(5000);
	clickElement("Approve1");
	enterText("Remarks","Approved"+sChargeCode);
	clickElement("APPROVE Remarks");		
	//Log.info("After Approve = " + getText("Charge Code Status",sChargeCode));
	}
	
	@And("^I verify that Charge Code is \"(.*)\" successfully$")
	public void I_verify_that_Charge_Code_status(String sStatus) throws Exception{
		Thread.sleep(5000);
		clickElement("CLEARFILTER");
		Thread.sleep(5000);
	clickElement("Charge Code Filter");
	Thread.sleep(5000);
	    enterText("Charge Code Filter TextBox",sChargeCode+"\n");	
	    Thread.sleep(5000);
	    Log.info("Status is "+sStatus+"\n"+"Charge Code"+sChargeCode);
	    Assert.assertTrue("Charge Code Status "+sStatus+" is not found for "+sChargeCode, getText("Charge Code Status",sChargeCode).equals(sStatus));
	    temp.put("chargeCodeStatus", sStatus);
		CSAutomationHooks.chargeJSONData.put(sChargeName, temp);
	
	} 
}

	


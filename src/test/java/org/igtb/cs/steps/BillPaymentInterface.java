package org.igtb.cs.steps;
import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.util.SystemOutLogger;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.DatabaseConnection;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVWriter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import edu.emory.mathcs.backport.java.util.Collections;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class BillPaymentInterface extends BaseFunctions {
	
	TestContext testContext;
	private Response response;
	private String sBillPayTriggerName = null; 
	private RequestSpecification request;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> BillPayExcelData = null;
	private Map<String,String> BillPayContent = null;
	private String sResponse = null;
	private ExcelUtils BillerParameterExcel = new ExcelUtils();
	private Map<String,Map<String,String>> BillParameterExcelData = null;
	private ExcelUtils BillerCategoryExcel = new ExcelUtils();
	private ExcelUtils BillCategoryExcel = new ExcelUtils();
	private ExcelUtils BillerPayRegistrationExcel = new ExcelUtils();
	private ExcelUtils BillerFetchBillExcel = new ExcelUtils();
	private ExcelUtils BillPaymentExcel=new ExcelUtils();
	private Map<String,Map<String,String>> BillerCategoryExcelData = null;
	private Map<String,Map<String,String>> BillCategoryExcelData = null;
	private Map<String,String> BillPayParameterContent = null;
	private Map<String,String> BillPayCategoryContent = null;
	private Map<String,String> BillPayRegistrationContent = null;
	private Map<String,String> BillFetchBillContent = null;
	private Map<String,String> BillCategoryContent = null;
	private Map<String,String>  BillPaymentContent=null;
	private Map<String,Map<String,String>> BillPayRegistrationExcelData = null;
	private Map<String,Map<String,String>> BillFetchBillExcelData = null;
	private Map<String,Map<String,String>> BillPaymentExcelData=null;
	private String BillerListSchema = null;
	private String  BillerListFailedSchema = null;
	private String BillerListFailedBnkSchema = null;
	private String BillerParameterSchema = null;
	private String BillerCategoryServiceTypeSchema = null;
	private String BillerPayerRegistrationSchema = null;
	private int nodes =0;
	private Map<String,String> temp =null;
	private String payerInternalId = null;
	private String fileName;
	private String sBatchNumber;
	private String sBillPayNumber;
	private List<Object> paramlist=null;
	
	public BillPaymentInterface(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "BillerList");
		BillPayExcelData = excel.getExcelDataIntoHashMap();		
		
		BillerParameterExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Biller Parameter");
		BillParameterExcelData = BillerParameterExcel.getExcelDataIntoHashMap();	
		
		BillerCategoryExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Biller Category");
		BillerCategoryExcelData = BillerCategoryExcel.getExcelDataIntoHashMap();
		
		BillerPayRegistrationExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Payer Registration");
		BillPayRegistrationExcelData = BillerPayRegistrationExcel.getExcelDataIntoHashMap();
		
		BillerFetchBillExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Fetch");
		BillFetchBillExcelData = BillerFetchBillExcel.getExcelDataIntoHashMap();
		

		BillPaymentExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Payment");
		BillPaymentExcelData = BillPaymentExcel.getExcelDataIntoHashMap();
		
		BillCategoryExcel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Category");
		BillCategoryExcelData = BillCategoryExcel.getExcelDataIntoHashMap();
	}
	
	

	
	@Given("^I trigger the Biller List Processing \"([^\"]*)\"$")
	public void i_trigger_the_Biller_List_Processing(String sBillPayTriggerName) throws Exception{
		File schemaFile = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerListSchema.json");
		File schemaFailedFile = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerListFailSchema.json");
		File schemaFailedFileBnkEntity = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerListFailSchemaBankEntity.json");
		
		String SchemaPath = schemaFile.getPath();
		String SchemaFailPath = schemaFailedFile.getPath();
		String SchemaBnkFailPath = schemaFailedFileBnkEntity.getPath();
		
		final Charset charset = Charset.defaultCharset();
		BillerListSchema = readFile(SchemaPath,charset); //BillerList Schema String
		BillerListFailedSchema = readFile(SchemaFailPath,charset);
		BillerListFailedBnkSchema = readFile(SchemaBnkFailPath,charset);
		
		this.sBillPayTriggerName = sBillPayTriggerName;
		BillPayContent = BillPayExcelData.get(sBillPayTriggerName);
		String outString = createBillerListRequest();
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString); //Request Creation
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT3"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT3")); //Request Posting
		System.out.println(response.asString());
		sResponse =response.asString(); //BillerList Response String
		
	   
	   
	}
	
	@Given("^I trigger the Parameter Field Processing \"([^\"]*)\"$")
	public void i_trigger_the_Parameter_Field_Processing(String sBillPayTriggerName) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		
		File schemaFile = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerParameterSchema.json");
		String SchemaPath = schemaFile.getPath();
		final Charset charset = Charset.defaultCharset();
		BillerParameterSchema = readFile(SchemaPath,charset); //BillerParameter Schema String
		
		
		this.sBillPayTriggerName = sBillPayTriggerName;
		BillPayParameterContent = BillParameterExcelData.get(sBillPayTriggerName);
		String outString = createBillerParameterRequest();
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT4"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT4"));
		System.out.println(response.asString());
		sResponse = response.asString(); //BillerParameter Response String
		
	}
	
	
	@Given("^I trigger the Biller Category Service Type Processing \"([^\"]*)\"$")
	public void i_trigger_the_Biller_Category_Service_Type_Processing(String sBillPayTriggerName) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    
		File schemaFile = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerCategoryServiceTypeSchema.json");
		String SchemaPath = schemaFile.getPath();
		final Charset charset = Charset.defaultCharset();
		BillerCategoryServiceTypeSchema = readFile(SchemaPath,charset); //BillerParameter Schema String
		
		this.sBillPayTriggerName = sBillPayTriggerName;
		BillPayCategoryContent = BillerCategoryExcelData.get(sBillPayTriggerName);
		String outString = createBillerCategoryServiceTypeRequest();
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT5"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT5"));
		System.out.println(response.asString());
		sResponse = response.asString(); //BillerParameter Response String
	}
	
	@When("^I trigger the Payer Registration Processing \"([^\"]*)\"$")
	public void i_trigger_the_Payer_Registration_Processing(String sBillPayTriggerName) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		File schemaFile = new File(AutomationHooks.RESOURCES_PATH+"/APITest/TestSchema/BillerPayerRegistrationSchema.json");
		String SchemaPath = schemaFile.getPath();
		final Charset charset = Charset.defaultCharset();
		BillerPayerRegistrationSchema = readFile(SchemaPath,charset); 
		
		
		this.sBillPayTriggerName = sBillPayTriggerName;
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
		BillPayRegistrationContent = BillPayRegistrationExcelData.get(sBillPayTriggerName);
		
		String outString = createPayerRegistrationRequest();
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT6"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT6"));
		System.out.println(response.asString());
		
		sResponse = response.asString(); //BillerParameter Response String
		payerInternalId = getPayerInternalID(sResponse);
		temp.put("payerInternalId",payerInternalId);
		
		
		
	}
	@Given("^I trigger Online Bill Fetch for processing \"([^\"]*)\"$")
	public void i_trigger_Online_Bill_Fetch_for_processing(String sBillFetchTriggerName) throws Throwable {
		this.sBillPayTriggerName = sBillFetchTriggerName;
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
		 BillFetchBillContent = BillFetchBillExcelData.get(sBillPayTriggerName); 
		String outString = createBillFetchRequest();
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT8"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT8"));
		System.out.println(response.asString());
		
		sResponse = response.asString(); //BillerParameter Response String
		
	}
	
	@Given("^I verify the the status response message for Online Bill Fetch is \"([^\"]*)\"$")
	public void i_verify_the_the_status_response_message_for_Online_Bill_Fetch_is(String Status) throws Throwable {
		JsonPath js= new JsonPath(sResponse);
		//String status = extractJSON(sResponse);
		String status=js.get("payload.status");
		System.out.println("Bill Fetch Status : "+status);
		Assert.assertEquals(Status, status);
	}
	
	@Given("^I trigger Bill Payment for processing \"([^\"]*)\"$")
	public void i_trigger_Bill_Payment_for_processing(String sBillPaymentTriggerName) throws Throwable {
		this.sBillPayTriggerName = sBillPaymentTriggerName; 
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
	  BillPaymentContent= BillPaymentExcelData.get(sBillPayTriggerName); 
	  
	  sBatchNumber = "BTCH"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
	  System.out.println("Batch No for Payment is => "+sBatchNumber);
	  DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PAYMENT_BATCH, sBatchNumber,BillPaymentContent.get("PRODUCT"),dateformat(BillPaymentContent.get("REQUEST_TIME")),BillPaymentContent.get("STATUS"));  
		temp.put("BATCH_NMBR",sBatchNumber);
		sBillPayNumber = "PAY"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PAYMENT_TXN,sBatchNumber,sBillPayNumber,BillPaymentContent.get("BILL_REFERENCE_NMBR"),BillPaymentContent.get("PAYMENT_REFERENCE_NMBR"),BillPaymentContent.get("BANK_ENTITY_ID"),temp.get("billercategorycode"),temp.get("ServiceTypeCode2"),temp.get("BillerMaintenanceCode"),BillPaymentContent.get("PAYMENT_CCY"),BillPaymentContent.get("BILL_PRESENTMENT_AMNT"),BillPaymentContent.get("BILL_PAYMENT_AMNT"),BillPaymentContent.get("PAYMENT_REMARKS"),dateformat(BillPaymentContent.get("PAYMENT_VALUE_DATE")),dateformat(BillPaymentContent.get("PAYMENT_POSTING_DATE")),BillPaymentContent.get("CIF_GUID"),BillPaymentContent.get("DEBIT_ACCT_NMBR"),BillPaymentContent.get("DEBIT_ACCT_CCY"));
	
		
DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PARAMETERS,temp.get("billercategorycode"),temp.get("ServiceTypeCode2"),temp.get("BillerMaintenanceCode"));
		List<Object> paramid =  DatabaseConnection.getInstance().getResultSetToMap().get("PARAM_ID");
		for(int i=0;i<paramid.size();i++)
		{
			
			DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PAYMENT_PARAM,sBatchNumber,sBillPayNumber,BillPaymentContent.get("BILL_REFERENCE_NMBR"),paramid.get(i).toString());
			
		}
		Map<String,String> data = new LinkedHashMap<>();
		data.put("product", BillPaymentContent.get("PRODUCT"));
		data.put("batchNmbr", sBatchNumber);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT9"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT9"));
		System.out.println(response.asString());
		
		
		
	}
	
	@Given("^I verify the the status response message for Bill Payment is \"([^\"]*)\"$")
	public void i_verify_the_the_status_response_message_for_Bill_Payment_is(String STATUS) throws Throwable {
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PAYMENT_TXN1,sBatchNumber,sBillPayNumber);
		//List<Object> lst = DatabaseConnection.getInstance().getResultSetToMap().get("STATUS");
		String status=DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString();
		
		System.out.println("BILL PAYMNET STATUS=>"+status);
		
		Thread.sleep(2000);
		String status1=DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString();
		System.out.println("BILL PAYMNET STATUS=>"+status1);
		Assert.assertEquals(STATUS, status1);
	}
	@Given("^I trigger Bill Presentment for processing \"([^\"]*)\"$")
	public void i_trigger_Bill_Presentment_for_processing(String sBillPayTriggerName) throws Exception{
		//getCollections();
		
		//TransferFile t = new TransferFile();
		//t.TransferFiles("C:\\Test\\package.txt", "/usr1/SIR15244/CS/samaya");
		BillPayRegistrationContent = BillPayRegistrationExcelData.get(sBillPayTriggerName);
		BillCategoryContent = BillCategoryExcelData.get(sBillPayTriggerName);
		this.sBillPayTriggerName = sBillPayTriggerName;
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
		ArrayList<String> firstLine = new ArrayList<String>();
		firstLine.add("H");
		firstLine.add("2019-06-15T15:39:06.154Z");
		firstLine.add("THAILAND");
		firstLine.add(temp.get("billercategorycode"));
		String lineOne = firstLine.toString();
		//writeDataForCustomSeperatorCSV("C:\\Test\\package.txt",lineOne);
		
		 // first create file object for file placed at location 
	     // specified by filepath 
		 fileName = "BILL_PAY"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		 System.out.println(fileName);
	     File file = new File(fileName+".txt"); 
	     
	    try { 
	         // create FileWriter object with file as parameter 
	         FileWriter outputfile = new FileWriter(file); 
	   
	         // create CSVWriter with '|' as separator 
	         CSVWriter writer = new CSVWriter(outputfile, '|', 
	                                          CSVWriter.NO_QUOTE_CHARACTER, 
	                                          CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
	                                          CSVWriter.DEFAULT_LINE_END); 
	         
	         // create a List which contains String array 
	         List<String[]> data = new ArrayList<String[]>(); 
	         //data.add(new String[] {lineOne});
	         data.add(new String[] { "H",fileName, currentDate(),"THAILAND",temp.get("BillerMaintenanceCode") }); 
	         data.add(new String[] { "CS",temp.get("billercategorycode"), temp.get("ServiceTypeCode2") });
	         data.add(new String[] { "BDTL","1110000231","Test0123456789",BillPayRegistrationContent.get("DTACMOBILENUMBER"),BillPayRegistrationContent.get("RELATIONSHIPNUMBER"),
	        		 					BillPayRegistrationContent.get("BILLPERIOD"),BillPayRegistrationContent.get("ZONE"),BillPayRegistrationContent.get("BILLNUMBER"),
	        		 					dateformat(BillPayRegistrationContent.get("BILLDATE")),dateformat(BillPayRegistrationContent.get("BILLDUEDATE")),BillPayRegistrationContent.get("BILLAMOUNT"),
	        		 					BillPayRegistrationContent.get("MONTHLYCHARGES"),BillPayRegistrationContent.get("USAGECHARGES"),BillPayRegistrationContent.get("TAXES")});
	         
	         writer.writeAll(data); 
	   
	         // closing writer connection 
	         writer.close(); 
	     } 
	     catch (IOException e) { 
	         // TODO Auto-generated catch block 
	         e.printStackTrace(); 
	     }
	    
	    TransferFile t = new TransferFile();
		t.TransferFiles(fileName+".txt", "/usr1/SIR17295/C24_WAR/billPresentment");
		
		String outString = createBillPresentmentRequest(BillCategoryContent);
		System.out.println(outString);
		request = given().header("Content-Type", "application/json").body(outString);
		
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT7"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT7"));
		System.out.println(response.asString());
		
		
	}
	
	@Given("^I verify the processing status$")
	public void i_verify_the_processing_status() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 Thread.sleep(4000);
		 DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_INFO, fileName);
		 String s = DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString();
		 System.out.println(s);
			
			Assert.assertEquals("SUCCESS", DatabaseConnection.getInstance().getResultSetToList().get(0).get("STATUS").toString());
			
	}
	


	@Then("^I verify the status response message for Biller List Processing$")
	public void i_verify_the_status_response_message_for_Biller_List_Processing() throws Exception {
		String status = extractJSON(sResponse);
		Assert.assertEquals(BillPayContent.get("Status"), status);
		
	}
	
	@Then("^I verify the status response message for Biller Parameter Processing$")
	public void i_verify_the_status_response_message_for_Biller_Parameter_Processing() throws Exception {
		
		//Validated JSON String with JSON Schema
		 if (ValidationUtils.isJsonValid(BillerParameterSchema, sResponse)){
		    	System.out.println("Valid String");
		    }else{
		    	System.out.println("NOT valid String");
		    }
		    Assert.assertTrue(ValidationUtils.isJsonValid(BillerParameterSchema, sResponse));
		    
		//   nodes= extractingJSON(sResponse);
		  // System.out.println(nodes);
		    
			
		
	}
	
	
	@Then("^I verify the status response message for Biller Category Service Type Processing$")
	public void i_verify_the_status_response_message_for_Biller_Category_Service_Type_Processing() throws Throwable {
	   
		//Validated JSON String with JSON Schema
		 if (ValidationUtils.isJsonValid(BillerCategoryServiceTypeSchema, sResponse)){
		    	System.out.println("Valid String");
		    }else{
		    	System.out.println("NOT valid String");
		    }
		    Assert.assertTrue(ValidationUtils.isJsonValid(BillerCategoryServiceTypeSchema, sResponse));
	}
	
	@Then("^I verify the status response message for Payer Registration$")
	public void i_verify_the_status_response_message_for_Payer_Registration() throws Exception {
		
		//Validated JSON String with JSON Schema
		 if (ValidationUtils.isJsonValid(BillerPayerRegistrationSchema, sResponse)){
		    	System.out.println("Valid String");
		    }else{
		    	System.out.println("NOT valid String");
		    }
		    Assert.assertTrue(ValidationUtils.isJsonValid(BillerPayerRegistrationSchema, sResponse));
		    
		  // nodes= extractingJSON(sResponse);
		  // System.out.println(nodes);
		    clickElement("REFRESH");
		    Thread.sleep(2000);
		    clickElement("Payer Internal Id Filter");
			Thread.sleep(2000);
			enterText("Payer Internal Id Filter TextBox",temp.get("payerInternalId")+"\n");	
			Thread.sleep(2000);
			Assert.assertTrue("Payer Internal id" +payerInternalId,getText("Payer Registration Status",payerInternalId).equals("Approved"));
	}
	
	
	private String createBillerListRequest() throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("id", BillPayContent.get("id"));
		data.put("eventVersion", BillPayContent.get("eventVersion"));
		
		
		Map<String,Object> context = new LinkedHashMap<>();
		context.put("channelSeqId",BillPayContent.get("channelSeqId"));
		context.put("domainSeqId",BillPayContent.get("domainSeqId"));
		context.put("eventTime",BillPayContent.get("eventTime"));
		
		Map<String,String> eventType = new LinkedHashMap<>();
		eventType.put("eventCategory",BillPayContent.get("eventCategory"));
		eventType.put("serviceKey",BillPayContent.get("serviceKey"));
		eventType.put("outcomeCategory",BillPayContent.get("outcomeCategory"));
		eventType.put("status",BillPayContent.get("status"));
		eventType.put("requestType",BillPayContent.get("requestType"));
		eventType.put("format",BillPayContent.get("format"));
		context.put("eventType", eventType);
		
		Map<String,String> eventSource = new LinkedHashMap<>();
		eventSource.put("sourceIdentity",BillPayContent.get("sourceIdentity"));
		eventSource.put("region",BillPayContent.get("region"));
		eventSource.put("country",BillPayContent.get("country"));
		
		context.put("eventSource", eventSource);
		data.put("context", context);
				
		Map<String,String> payload = new LinkedHashMap<>();
		payload.put("msgId",BillPayContent.get("msgId"));
		payload.put("eventReqTime",BillPayContent.get("eventReqTime"));
		payload.put("eventResTime",BillPayContent.get("eventResTime"));
		payload.put("bankEntityId",BillPayContent.get("bankEntityId"));
		
		data.put("payload", payload);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		
		return outString;
		
	}
	
	private String createBillerParameterRequest() throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("id", BillPayParameterContent.get("id"));
		data.put("eventVersion", BillPayParameterContent.get("eventVersion"));
		
		
		Map<String,Object> context = new LinkedHashMap<>();
		context.put("channelSeqId",BillPayParameterContent.get("channelSeqId"));
		context.put("domainSeqId",BillPayParameterContent.get("domainSeqId"));
		context.put("eventTime",BillPayParameterContent.get("eventTime"));
		
		Map<String,String> eventType = new LinkedHashMap<>();
		eventType.put("eventCategory",BillPayParameterContent.get("eventCategory"));
		eventType.put("serviceKey",BillPayParameterContent.get("serviceKey"));
		eventType.put("outcomeCategory",BillPayParameterContent.get("outcomeCategory"));
		eventType.put("status",BillPayParameterContent.get("status"));
		eventType.put("requestType",BillPayParameterContent.get("requestType"));
		eventType.put("format",BillPayParameterContent.get("format"));
		context.put("eventType", eventType);
		
		Map<String,String> eventSource = new LinkedHashMap<>();
		eventSource.put("sourceIdentity",BillPayParameterContent.get("sourceIdentity"));
		eventSource.put("region",BillPayParameterContent.get("region"));
		eventSource.put("country",BillPayParameterContent.get("country"));
		
		context.put("eventSource", eventSource);
		data.put("context", context);
				
		Map<String,String> payload = new LinkedHashMap<>();
		payload.put("msgId",BillPayParameterContent.get("msgId"));
		payload.put("eventReqTime",BillPayParameterContent.get("eventReqTime"));
		payload.put("eventResTime",BillPayParameterContent.get("eventResTime"));
		payload.put("bankEntityId",BillPayParameterContent.get("bankEntityId"));
		payload.put("billerCategoryCode", BillPayParameterContent.get("billerCategoryCode"));
		payload.put("serviceTypeCode", BillPayParameterContent.get("serviceTypeCode"));
		payload.put("billerId", BillPayParameterContent.get("billerId"));
		data.put("payload", payload);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		return outString;
		
	}
	
	private String createBillerCategoryServiceTypeRequest() throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("id", BillPayCategoryContent.get("id"));
		data.put("eventVersion", BillPayCategoryContent.get("eventVersion"));
		
		
		Map<String,Object> context = new LinkedHashMap<>();
		context.put("channelSeqId",BillPayCategoryContent.get("channelSeqId"));
		context.put("domainSeqId",BillPayCategoryContent.get("domainSeqId"));
		context.put("eventTime",BillPayCategoryContent.get("eventTime"));
		
		Map<String,String> eventType = new LinkedHashMap<>();
		eventType.put("eventCategory",BillPayCategoryContent.get("eventCategory"));
		eventType.put("serviceKey",BillPayCategoryContent.get("serviceKey"));
		eventType.put("outcomeCategory",BillPayCategoryContent.get("outcomeCategory"));
		eventType.put("status",BillPayCategoryContent.get("status"));
		eventType.put("requestType",BillPayCategoryContent.get("requestType"));
		eventType.put("format",BillPayCategoryContent.get("format"));
		context.put("eventType", eventType);
		
		Map<String,String> eventSource = new LinkedHashMap<>();
		eventSource.put("sourceIdentity",BillPayCategoryContent.get("sourceIdentity"));
		eventSource.put("region",BillPayCategoryContent.get("region"));
		eventSource.put("country",BillPayCategoryContent.get("country"));
		
		context.put("eventSource", eventSource);
		data.put("context", context);
				
		Map<String,String> payload = new LinkedHashMap<>();
		payload.put("msgId",BillPayCategoryContent.get("msgId"));
		payload.put("eventReqTime",BillPayCategoryContent.get("eventReqTime"));
		payload.put("eventResTime",BillPayCategoryContent.get("eventResTime"));
		payload.put("bankEntityId",BillPayCategoryContent.get("bankEntityId"));
		
		data.put("payload", payload);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		
		return outString;
		
	}
	
	
	private String createPayerRegistrationRequest() throws Exception 
	{    
		Map<String,Object> data = new LinkedHashMap<>();
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
		data.put("id", BillPayRegistrationContent.get("id"));
		data.put("eventVersion", BillPayRegistrationContent.get("eventVersion"));
		
		
		Map<String,Object> context = new LinkedHashMap<>();
		context.put("channelSeqId",BillPayRegistrationContent.get("channelSeqId"));
		context.put("domainSeqId",BillPayRegistrationContent.get("domainSeqId"));
		context.put("eventTime",BillPayRegistrationContent.get("eventTime"));
		
		Map<String,String> eventType = new LinkedHashMap<>();
		eventType.put("eventCategory",BillPayRegistrationContent.get("eventCategory"));
		eventType.put("serviceKey",BillPayRegistrationContent.get("serviceKey"));
		eventType.put("outcomeCategory",BillPayRegistrationContent.get("outcomeCategory"));
		eventType.put("status",BillPayRegistrationContent.get("status"));
		eventType.put("requestType",BillPayRegistrationContent.get("requestType"));
		eventType.put("format",BillPayRegistrationContent.get("format"));
		context.put("eventType", eventType);
		
		Map<String,String> eventSource = new LinkedHashMap<>();
		eventSource.put("sourceIdentity",BillPayRegistrationContent.get("sourceIdentity"));
		eventSource.put("region",BillPayRegistrationContent.get("region"));
		eventSource.put("country",BillPayRegistrationContent.get("country"));
		
		context.put("eventSource", eventSource);
		data.put("context", context);
				
		Map<String,Object> payload = new LinkedHashMap<>();
		payload.put("msgId",BillPayRegistrationContent.get("msgId"));
		payload.put("eventReqTime",BillPayRegistrationContent.get("eventReqTime"));
		payload.put("eventResTime",BillPayRegistrationContent.get("eventResTime"));
		payload.put("bankEntityId",BillPayRegistrationContent.get("bankEntityId"));
		
		payload.put("billerId",temp.get("BillerMaintenanceCode"));
		payload.put("billerCategoryCode",temp.get("billercategorycode"));
		payload.put("serviceTypeCode",temp.get("ServiceTypeCode2"));
		
		
		payload.put("cifGuid",BillPayRegistrationContent.get("cifGuid"));
		payload.put("payerInternalId",BillPayRegistrationContent.get("payerInternalId"));
		payload.put("payerStatus",BillPayRegistrationContent.get("payerStatus"));
		payload.put("processingDate",dateformat(BillPayRegistrationContent.get("processingDate")));
		payload.put("makerId",BillPayRegistrationContent.get("makerId"));
		payload.put("makerDate",dateformat(BillPayRegistrationContent.get("makerDate")));
		payload.put("checkerId",BillPayRegistrationContent.get("checkerId"));
		payload.put("checkerDate",dateformat(BillPayRegistrationContent.get("checkerDate")));
		temp.put("cif-guid",BillPayRegistrationContent.get("cifGuid"));
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PARAMETERS,temp.get("billercategorycode"),temp.get("ServiceTypeCode2"),temp.get("BillerMaintenanceCode"));
		
		List<Object> payerreg =  DatabaseConnection.getInstance().getResultSetToMap().get("APPLICABLE_PAYER_REGN");
		List<Object> paramid =  DatabaseConnection.getInstance().getResultSetToMap().get("PARAM_ID");
		
		Map<String,String> reqParameters = new LinkedHashMap<>();
		for(int i =0; i< payerreg.size();i++)
		{
		if(payerreg.get(i).toString().equals("M")||payerreg.get(i).toString().equals("O"))
		{
			
			
			
			reqParameters.put(paramid.get(i).toString(), BillPayRegistrationContent.get(paramid.get(i).toString()));
		}
		}
		
		
			
		
		payload.put("reqParameters",reqParameters);
		
		
		data.put("payload", payload);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		
		return outString;
	}
	private String createBillFetchRequest() throws Exception
	{
		Map<String,Object> data = new LinkedHashMap<>();
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillPayTriggerName);
		data.put("id", BillFetchBillContent.get("id"));
		data.put("eventVersion", BillFetchBillContent.get("eventVersion"));
		
		
		Map<String,Object> context = new LinkedHashMap<>();
		context.put("channelSeqId",BillFetchBillContent.get("channelSeqId"));
		context.put("domainSeqId",BillFetchBillContent.get("domainSeqId"));
		context.put("eventTime",BillFetchBillContent.get("eventTime"));
		
		Map<String,String> eventType = new LinkedHashMap<>();
		eventType.put("eventCategory",BillFetchBillContent.get("eventCategory"));
		eventType.put("serviceKey",BillFetchBillContent.get("serviceKey"));
		eventType.put("outcomeCategory",BillFetchBillContent.get("outcomeCategory"));
		eventType.put("status",BillFetchBillContent.get("status"));
		eventType.put("requestType",BillFetchBillContent.get("requestType"));
		eventType.put("format",BillFetchBillContent.get("format"));
		context.put("eventType", eventType);
		
		Map<String,String> eventSource = new LinkedHashMap<>();
		eventSource.put("sourceIdentity",BillFetchBillContent.get("sourceIdentity"));
		eventSource.put("region",BillFetchBillContent.get("region"));
		eventSource.put("country",BillFetchBillContent.get("country"));
		
		context.put("eventSource", eventSource);
		data.put("context", context);
		
		Map<String,Object> payload = new LinkedHashMap<>();
		payload.put("msgId",BillFetchBillContent.get("msgId"));
		payload.put("eventReqTime",BillFetchBillContent.get("eventReqTime"));
		payload.put("eventResTime",BillFetchBillContent.get("eventResTime"));
		payload.put("bankEntityId",BillFetchBillContent.get("bankEntityId"));
		
		payload.put("billerId",temp.get("BillerMaintenanceCode"));
		payload.put("billerCategoryCode",temp.get("billercategorycode"));
		payload.put("serviceTypeCode",temp.get("ServiceTypeCode2"));
		
		payload.put("cifGuid",BillFetchBillContent.get("cifGuid"));
		
DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.BILL_PARAMETERS,temp.get("billercategorycode"),temp.get("ServiceTypeCode2"),temp.get("BillerMaintenanceCode"));
		
		List<Object> fetchreg =  DatabaseConnection.getInstance().getResultSetToMap().get("APPLICABLE_FETCH_BILLDATA");
		List<Object> paramid =  DatabaseConnection.getInstance().getResultSetToMap().get("PARAM_ID");
		
		Map<String,String> reqParameters = new LinkedHashMap<>();
		for(int i =0; i< fetchreg.size();i++)
		{
		if(fetchreg.get(i).toString().equals("M")||fetchreg.get(i).toString().equals("O"))
		{
			
			
			
			reqParameters.put(paramid.get(i).toString(), BillFetchBillContent.get(paramid.get(i).toString()));
		}
		}
		
		
			
		
		payload.put("reqParameters",reqParameters);
		
		
		data.put("payload", payload);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		
		return outString;
		
	}
	private String createBillPresentmentRequest(Map<String,String> billCategory) throws Exception 
	{
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("entityType", "B");
		data.put("entityId", "THAILAND");
		data.put("interfaceName", "BILL_PRESENTMENT");
		
		Map<String,Object> details = new LinkedHashMap<>();
		details.put("orgFileLocation", "");
		details.put("orgFileName", "");
		details.put("processFileLocation", "/usr1/SIR17295/C24_WAR/billPresentment/");
		details.put("processFileName", fileName+".txt");
		
		
		List<Map<String,String>> list = new ArrayList<>();
		
		
		
		
		String FieldType=billCategory.get("Field Id");
		String[] sFieldType=FieldType.split(",");
		
		/*for(int k=0;k<sFieldType.length;k++)
		//for(String STFieldType:sFieldType)
		{
			Map<String,String> attributes = new LinkedHashMap<>();
			attributes.put("key",sFieldType[k]);
			attributes.put("value",BillPayRegistrationContent.get(sFieldType[k]));
			
			//list.add(attributes);
			list.add(k, attributes);
			//System.out.println(STFieldType);
		}*/
		Map<String,String> attributes = new LinkedHashMap<>();
		attributes.put("key","");
		attributes.put("value","");
		list.add(attributes);
		details.put("attributes", list);
		data.put("details", details);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		
		return outString;
	}
	
	private int extractingJSON(String jsonStr) throws Exception {
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONObject payload = jsonObj.getJSONObject("payload");
		JSONArray message = payload.getJSONArray("message");
		int i = message.length();
    	for(i=0;i<message.length();i++)
		{
		JSONObject json_obj = message.getJSONObject(i);
		
		String str1 = json_obj.getString("billerCategoryCode");
		System.out.println(i+" "+str1);
		}
		i = i+1;
		return i;	
	}
	
	private String extractJSON(String jsonStr) throws Exception {
		
		String status = null;
		if(BillPayContent.get("ProcessingStatus").equals("SUCCESS"))
		{
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONObject payload = jsonObj.getJSONObject("payload");
		status = payload.getString("status");
		System.out.println(status);
		}
		if(BillPayContent.get("ProcessingStatus").equals("FAILRESPONSE"))
		{
		JSONObject jsonObj = new JSONObject(jsonStr);
		JSONObject payload = jsonObj.getJSONObject("payload");
		status = payload.getString("status");
		System.out.println(status);
		}
		if(BillPayContent.get("ProcessingStatus").equals("FAILED"))
		{
		JSONObject jsonObj = new JSONObject(jsonStr);
		status = jsonObj.getString("status");
		System.out.println(status);
		}
		return status;	
		
	}
	
	 public  void writeToTextFile(String fileName, String content) throws IOException {
	        Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
	    }
	
	  String readFile(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  return new String(encoded, encoding);
			}
	 
	  String dateformat(String str)
	 {
		 String outstring = str.replace("/","-");
				 return outstring;
	 }
	
	 public  String getPayerInternalID(String str) 
	 {
		
		 String internalID=null;
		 
		 JsonParser parser = new JsonParser();
		 JsonElement jsonTree = parser.parse(str);
		 JsonObject jsonObject = jsonTree.getAsJsonObject();
		 if(jsonObject.has("id"))
		 {
		 JsonElement payload = jsonObject.get("payload");
		 
		 String str1 = payload.toString();
		 JsonElement jsonTree1 = parser.parse(str1);
		 JsonObject jsonObject1 = jsonTree1.getAsJsonObject();
		 JsonElement payload1 = jsonObject1.get("payload");
		 
		 String str2 = payload1.toString();
		 JsonElement jsonTree2 = parser.parse(str2);
		 JsonObject jsonObject2 = jsonTree2.getAsJsonObject();
		 JsonElement payerinter = jsonObject2.get("payerInternalId");
		 internalID = payerinter.toString();
		 System.out.println(internalID);
		 internalID = internalID.replace("\"", "");	
		 System.out.println(internalID);
		 }
		return internalID;
		
			
	 }
	 
	 public void getCollections()
	 {
		 ArrayList<String> list = new ArrayList<String>();
		 list.add("Ravi");
		 list.add("Vijay");
		 list.add("Ajay");
		 list.add("Uday");
		 list.add(2,"Naeem");
		 System.out.println(list);
		 System.out.println("\n ==================================\n");
		 for(String str: list)
			 System.out.println(str);
		 
		 System.out.println("\n ==================================\n");
		 Iterator itr = list.iterator();
		 while(itr.hasNext())
		 {
			 System.out.println(itr.next());
		 }
		 System.out.println("\n ==================================\n");
		 Collections.sort(list);
		 for(String str: list)
			 System.out.println(str);
		 
		 System.out.println("\n ==================================\n");
		 Collections.reverse(list);
		 for(String str: list)
			 System.out.println(str);
	 }
	 
	 public static void writeDataForCustomSeperatorCSV(String filePath, String lineOne) 
	 { 
	   
	     // first create file object for file placed at location 
	     // specified by filepath 
		 
	     File file = new File(filePath); 
	    
	   
	     try { 
	         // create FileWriter object with file as parameter 
	         FileWriter outputfile = new FileWriter(file); 
	   
	         // create CSVWriter with '|' as separator 
	         CSVWriter writer = new CSVWriter(outputfile, '|', 
	                                          CSVWriter.NO_QUOTE_CHARACTER, 
	                                          CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
	                                          CSVWriter.DEFAULT_LINE_END); 
	         
	         // create a List which contains String array 
	         List<String[]> data = new ArrayList<String[]>(); 
	         //data.add(new String[] {lineOne});
	         //data.add(new String[] { "Aman", temp.get("billercategorycode"), "620" }); 
	        // data.add(new String[] { "Suraj", "10", "630" }); 
	         writer.writeAll(data); 
	   
	         // closing writer connection 
	         writer.close(); 
	     } 
	     catch (IOException e) { 
	         // TODO Auto-generated catch block 
	         e.printStackTrace(); 
	     } 
	 }
	 
	 public String currentDate()
	 {
		 String dt,tm,str;
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		 DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm:ss");
		 LocalDateTime now = LocalDateTime.now();
		 dt = dtf.format(now);
		 tm = dtf1.format(now);
		 str = dt+"T"+tm+".000Z";
		 return str;
	 }
	 
	 
	
	

}

package org.igtb.cs.steps;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BillerMaintenance extends BaseFunctions {
	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> BillerMaintenanceExcelData = null;
	private String sBillerMaintenance = null; 
	private String sBillerMaintenanceCode = null;
	private String sBillerMaintenanceName = null;
	private Map<String,String> temp =null;
	 
	
	public BillerMaintenance(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Bill Category");
		BillerMaintenanceExcelData = excel.getExcelDataIntoHashMap();
	}
	
	@When("^I add New Biller \"([^\"]*)\"$")
	public void i_add_New_Biller(String sBillerMaintenance) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		this.sBillerMaintenance = sBillerMaintenance;
		
		Map<String,String> BillMaintenanceContent = BillerMaintenanceExcelData.get(sBillerMaintenance);
		temp = CSAutomationHooks.billpayJSONData.get(this.sBillerMaintenance);
		
		sBillerMaintenanceCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		sBillerMaintenanceName = sBillerMaintenance+sBillerMaintenanceCode;
		clickElement("Add New Biller Maintenance");
		enterText("Biller Id",sBillerMaintenanceCode);
		enterText("Biller Name",sBillerMaintenanceName);
		enterText("Postal Code",BillMaintenanceContent.get("Postal Code"));
		enterText("State",BillMaintenanceContent.get("State"));
		Thread.sleep(1000);
		clickElement("SelectValueState");
		enterText("City",BillMaintenanceContent.get("City"));
		Thread.sleep(1000);
		clickElement("SelectValueCity");
		enterText("AddressLine1",BillMaintenanceContent.get("Address Line 1"));
		Thread.sleep(2000);
		clickElement("Is Collection Customer",BillMaintenanceContent.get("Is Collection Customer"));
		if(BillMaintenanceContent.get("Is Collection Customer").equals("Yes"))
		{
			enterText("Customer Id",BillMaintenanceContent.get("Customer Id"));
			Thread.sleep(1000);
			clickElement("SelectValueCustomer");
		}
		
		
		if(isElementEnabled("Submit")){
			clickElement("Submit");
		}else{
			Assert.assertTrue("Error while adding Biller Category",false);
		}
		Thread.sleep(3000);
		temp.put("BillerMaintenanceCode", sBillerMaintenanceCode);
		temp.put("BillerMaintenanceName", sBillerMaintenanceName);
		//temp.put("BillerMaintenanceStatus", sStatus);
	}
	
	@When("^I verify that Biller is \"([^\"]*)\" successfully$")
	public void i_verify_that_Biller_is_successfully(String sStatus) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		clickElement("CLEARFILTER");
		Thread.sleep(2000);
		clickElement("Biller Id Filter");
		Thread.sleep(2000);
		enterText("Biller Id Filter TextBox",sBillerMaintenanceCode+"\n");	
		Thread.sleep(2000);
		Assert.assertTrue("Bill Category Code Status"+sStatus+" is not found for "+sBillerMaintenanceCode, getText("Biller Id Status",sBillerMaintenanceCode).equals(sStatus));
	    temp.put("BillerMaintenanceStatus", sStatus);
	}
	
	@Then("^I approve Biller$")
	public void i_approve_Biller() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		clickElement("Biller Id Filter");
		Thread.sleep(2000);
		enterText("Biller Id Filter TextBox",sBillerMaintenanceCode+"\n");
		Thread.sleep(2000);
		clickElement("Biller Id CheckBox",sBillerMaintenanceCode);
		clickElement("Approve1");
		Thread.sleep(2000);
		enterText("Remarks","Approved"+sBillerMaintenanceCode);
		clickElement("APPROVE Remarks");
		clickElement("REFRESH");
	}


}

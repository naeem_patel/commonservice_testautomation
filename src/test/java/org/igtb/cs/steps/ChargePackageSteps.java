package org.igtb.cs.steps;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChargePackageSteps  extends BaseFunctions {
	TestContext testContext;
	private ExcelUtils excel1 = new ExcelUtils();
	private Map<String,Map<String,String>> chargePkgExcelData = null;
	private String sChargePkgName = null; 
	private String sChargePkgCode = null; 
	private String sChargePkgDes = null;
	private Map<String,String> temp,temp1 =null;
	// private Map<String, String>  temp= new HashMap<>();
	
	public ChargePackageSteps(TestContext context) throws Exception {
		super(context);
       testContext = context;
		
		excel1.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls","Charge Package");
		
		chargePkgExcelData = excel1.getExcelDataIntoHashMap();
		
	}
		@When("^I add Charge Package for \"(.*)\"$") 
		public void I_add_Charge_Package(String sChargePkgName) throws Exception{


			this.sChargePkgName = sChargePkgName;
			
			Map<String,String> chgPkgContent = chargePkgExcelData.get(sChargePkgName);
			
			
			sChargePkgCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
			sChargePkgDes = sChargePkgName+sChargePkgCode;
			clickElement("Add New Charge Package");
			
			
			enterText("Package Code",sChargePkgCode);
		      enterText("Package Description",sChargePkgDes);
		     enterText("Start Date",chgPkgContent.get("Start Date"));
		      enterText("End Date",chgPkgContent.get("End Date"));
		     
		      String ChgName=chgPkgContent.get("Charge Name");
		     // temp = CSAutomationHooks.chargeJSONData.get(this.sChargePkgName);
		     String[] ChargeName=ChgName.split(",");
		      int i=1;
		      for(String CHName:ChargeName)
		     {
		    	  temp	= CSAutomationHooks.chargeJSONData.get(CHName);
		    	  clickElement("Add new Charge Rule");
		    	  Thread.sleep(2000);
		    	  selectElementFromList("Product-package",String.valueOf(i),chgPkgContent.get("Product"));
		    	  selectElementFromList("Event Type Pac",String.valueOf(i),chgPkgContent.get("Event"));
		    	  
		    	 waitForElementVisibility("Charge Code Pac", String.valueOf(i));
		    	  selectElementFromList("Charge Code Pac",String.valueOf(i),temp.get("chargeCode"));
		    	  
		    	  i++;
		    	  Thread.sleep(2000);
			      temp.put("packagecode", sChargePkgCode);
					temp.put("packagedesc", sChargePkgDes);
		    }
		    
		     /* Thread.sleep(2000);
		      temp.put("packagecode", sChargePkgCode);
				temp.put("packagedesc", sChargePkgDes);*/
		  	
		  	if(isElementEnabled("Submit")){
		  		clickElement("Submit");
		  	}else{
		  		Assert.assertTrue("Error while adding Charge Code",false);
		  	}
		      
		      
		  Thread.sleep(2000);
	
	}
	
		@Then("^I approve Charge Package$")
		public void I_approve_Charge_Package() throws Exception 
		{
			Thread.sleep(2000);
			clickElement("Package Code Filter");
			Thread.sleep(2000);
			enterText("Package Code Filter TextBox",sChargePkgCode+"\n");
			Thread.sleep(2000);
			clickElement("Package Code CheckBox",sChargePkgCode);
			clickElement("Approve1");
			enterText("Remarks","Approved"+sChargePkgCode);
			clickElement("APPROVE Remarks");
			
		}
		@And("^I verify that Charge Package is \"(.*)\" successfully$")
		public void I_verify_that_Charge_Package_status(String sStatus) throws Exception{
			
			Thread.sleep(2000);
			clickElement("CLEARFILTER");
			Thread.sleep(2000);
			clickElement("Package Code Filter");
			Thread.sleep(2000);
			    enterText("Package Code Filter TextBox",sChargePkgCode+"\n");	
			    Thread.sleep(2000);
			Assert.assertTrue("Package Code Status"+sStatus+" is not found for "+sChargePkgCode, getText("Package Code Status",sChargePkgCode).equals(sStatus));
		temp.put("PackageCodeStatus", sStatus);
			//CSAutomationHooks.chargeJSONData.put(sChargePkgName, temp);
		
		}
		
		
}
package org.igtb.cs.steps;

import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MessageTemplateSteps extends BaseFunctions{

	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> alertExcelData = null;
	private String sAlertMessageTemplateName = null; 
	private String sAlertMessageTemplateCode = null; 
	private String sAlertMessageDescription = null;
	private Map<String,String> temp =null;
	
	public MessageTemplateSteps(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Message Template");
		alertExcelData = excel.getExcelDataIntoHashMap();
	}
	
	@When("^I add Message Template for \"(.*)\"$")
	public void I_add_Message_Template(String sAlertMessageTemplateName) throws Exception{
		this.sAlertMessageTemplateName = sAlertMessageTemplateName;
		temp = CSAutomationHooks.alertJSONData.get(this.sAlertMessageTemplateName);
		Map<String,String> alertContent = alertExcelData.get(sAlertMessageTemplateName);
		sAlertMessageTemplateCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		sAlertMessageDescription = sAlertMessageTemplateName+sAlertMessageTemplateCode;
		temp.put("product", alertContent.get("Product"));
		temp.put("event", alertContent.get("Event"));
		temp.put("SMS", alertContent.get("SMS"));
		temp.put("Email", alertContent.get("Email"));
		temp.put("Fax", alertContent.get("Fax"));
		temp.put("Print", alertContent.get("Print"));
		temp.put("alertMessageTemplateCode", sAlertMessageTemplateCode);
		temp.put("alertMessageDescription", sAlertMessageDescription);
		
		clickElement("Add new message Template");
        enterText("Alert Message Template Code",sAlertMessageTemplateCode);
        enterText("Alert Message Description",sAlertMessageDescription);
        selectElementFromList("Product",alertContent.get("Product"));
        enterText("Event", alertContent.get("Event"));
        Thread.sleep(3000);
		clickElement("NG2-AUTO-COMPLETE");
		if(alertContent.get("SMS").equalsIgnoreCase("Yes")){
			clickElement("SMS Tab");
	        enterText("SMS Body",alertContent.get("SMS Body"));
//	        selectElementFromList("SMS Event Attributes", "BILL_DATE");
//			clickElement("SMS INSERT");
		}
		if(alertContent.get("Email").equalsIgnoreCase("Yes")){
			clickElement("Email Tab");
	        enterText("Email Subject",alertContent.get("Email Subject"));
//	        selectElementFromList("Email Subject Event Attributes", "RECOVERY_AMNT");
//			clickElement("Email Subject INSERT");	
	        enterText("Email Body",alertContent.get("Email Body"));
//	        selectElementFromList("Email Body Event Attributes", "BILLING_CYCLE_TYPE");
//			clickElement("Email Body INSERT");	
		}	
		if(isElementEnabled("Submit")){
			clickElement("Submit");
		}else{
			Assert.assertTrue("Error while adding Message Template",false);
		}
	}
	
	@Then("^I approve Message Template$")
	public void I_approve_Message_Template() throws Exception{
		Thread.sleep(3000);
		clickElement("Alert Message Template Filter");
		Thread.sleep(3000);
		enterText("Alert Message Template Filter TextBox",sAlertMessageTemplateCode+"\n");	
		Thread.sleep(3000);
//		Log.info("Before Approve = " + getText("Alert Message Template Status",sAlertMessageTemplateCode));
		clickElement("Alert Message Template Checkbox",sAlertMessageTemplateCode);
		Thread.sleep(3000);
		clickElement("APPROVE");
		Thread.sleep(3000);
		enterText("Remarks","Approved"+sAlertMessageTemplateCode);
		Thread.sleep(3000);
		clickElement("APPROVE Remarks");
		Thread.sleep(3000);
//		Log.info("After Approve = " + getText("Alert Message Template Status",sAlertMessageTemplateCode));
	}
	
	@And("^I verify that Message Template is \"(.*)\" successfully$")
	public void I_verify_that_Message_Template_status(String sStatus) throws Exception{
		Thread.sleep(3000);
		clickElement("Alert Message Template Filter");
		enterText("Alert Message Template Filter TextBox",sAlertMessageTemplateCode+"\n");	
		Thread.sleep(3000);
		Assert.assertTrue("Alert Message Template Status "+sStatus+" is not found for "+sAlertMessageTemplateCode, getText("Alert Message Template Status",sAlertMessageTemplateCode).equals(sStatus));
		Thread.sleep(3000);
		temp.put("messageTemplateStatus", sStatus);
		Thread.sleep(3000);
		CSAutomationHooks.alertJSONData.put(sAlertMessageTemplateName, temp);
		Thread.sleep(3000);
	} 
}

package org.igtb.cs.steps;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.Log;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class ARXLoginSteps extends BaseFunctions{

	TestContext testContext;

	public ARXLoginSteps(TestContext context){
		super(context);
		testContext = context;
	}

	@Given("^I am on Login Page$")
	public void I_am_on_Login_Page(){
		open_url();
	}
	
	@Given("^User Opens New Browser$")
	public void user_Opens_New_Browser() throws Exception  {
		
	}

	@Given("^I am on ARX Login Page$")
	public void I_am_on_Login_Page_for_ARX() throws Exception{
		open_url();
		switchToWindow("Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena");
	}

	@Given("^I switch to \"(.*)\" User$")
	public void I_switch_to_user(String sUser) throws Exception{
		clickElement("User");
		clickElement("User Link",sUser);
		Thread.sleep(5000);
		clickElement("CLEARFILTER");
		Thread.sleep(1000);
		clickElement("REFRESH");
		waitForRefresh();
	}

	@Given("^I am on Alerts Page$")
	public void I_am_on_Alerts_Page() throws Exception{
		clickElement("Alerts");
		clickElement("Message Template");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Alerts Subscription Page$")
	public void I_am_on_Alerts_Subscription_Page() throws Exception{
		
		clickElement("Alert Subscription");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Alert Monitoring Page$")
	public void i_am_on_Alert_Monitoring_Page() throws Exception  {
		clickElement("Alert Monitoring");
		waitForRefresh();
		
	}
	
	@Given("^I am on Alerts Package Page$")
	public void I_am_on_Alerts_Package_Page() throws Exception{
		
		clickElement("Alert Package");
		//clickElement("REFRESH");
		waitForRefresh();
	}

	private void waitForRefresh() {
		boolean bFlag = true;
		do{
			try{
				if(driver.getDriver().findElement(By.xpath("//div[@class='datagrid-spinner']")).isDisplayed()){
					Log.info("Waiting for refresh....");
					Thread.sleep(1000);
					continue;
				}else{
					bFlag = false;					
				}			
			}catch (Exception e) {
				bFlag = false;
			}
		}while(bFlag == true);
	}
	
	@Given("^I am on Charges Page$")
	public void I_am_on_Charges_Page() throws Exception{
		clickElement("Charges");
		clickElement("Charge Code Link");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	
	
	@Given("^I am on Charge Package Page$")
	public void I_am_on_Charges__Package_Page() throws Exception{
		clickElement("Charges");
		clickElement("Charge Package");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Bill Category Screen$")
	public void i_am_on_Bill_Category_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		clickElement("Bill Payment");
		clickElement("Biller Category Link");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Bill Maintenance Screen$")
	public void i_am_on_Bill_Maintenance_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		clickElement("Bill Payment");
		clickElement("Biller Maintenance Link");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Biller Service Configuration Screen$")
	public void i_am_on_Biller_Service_Configuration_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		clickElement("Bill Payment");
		clickElement("Biller-Service Configuration Link");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	@Given("^I am on Payer Registration Screen$")
	public void i_am_on_Payer_Registration_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		clickElement("Bill Payment");
		clickElement("Payer-Payee Registration Link");
		clickElement("REFRESH");
		waitForRefresh();
	}
	
	
	@Given("^I am on Tax Set Page$")
	public void i_am_on_Tax_Set_Page() throws Exception {
		clickElement("Charges");
		clickElement("Tax Set Link");
		clickElement("REFRESH");
		waitForRefresh();
	}

	@And("^I login with \"(.*)\" for ARX$")
	public void login(String sUser) throws Exception  {
		if(sUser.equalsIgnoreCase("Maker")){
			enterText("ARX_UserID","QCALLUSER104");
			enterText("ARX_Password","Int01$");
			clickElement("ARX_Login");
			if(isElementDisplayed("Kill Previous Session"))
				clickElement("Kill Previous Session");
			switchToWindow("Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN");
		}
		else if(sUser.equalsIgnoreCase("Checker")){
			enterText("ARX_UserID","QCALLUSER103");
			enterText("ARX_Password","Int01$");
			clickElement("ARX_Login");
			if(isElementDisplayed("Kill Previous Session"))
				clickElement("Kill Previous Session");
			switchToWindow("Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN");
		}
	}

	@And("^I select \"(.*)\" Web Application for ARX$")
	public void selectWorkspace(String sWorkspaceName) throws Exception  {
		clickElement(sWorkspaceName);
		switchToWindow("iGTB Digital - DTB");
		maximizeBrowser();
	}

	@Given("^I logout from application for ARX$")
	public void I_logout_from_application() throws Exception{
		switchToWindow("Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN");
		clickElement("ARX_Menu");
		clickElement("ARX_Logout");
		switchToWindow("Thanks for using Intellect from Intellect Design Arena LTD");
		clickElement("ARX_Close");
		switchToWindow("Welcome to Intellect ARX from Intellect Design Arena LTD.");
	}
	
	@Given("^I am on Calendar Master Screen$")
	public void i_am_on_Calendar_Master_Screen() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    clickElement("Calendar");
	}

}

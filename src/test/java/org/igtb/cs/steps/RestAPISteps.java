package org.igtb.cs.steps;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.DatabaseConnection;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.remote.server.handler.SendKeys;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAPISteps extends BaseFunctions{

	TestContext testContext;
	private Response response;
	private RequestSpecification request;
	private String sAlertTriggerName = null; 
	private String sAlertPackageCode = null; 
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> alertExcelData = null;
	private Map<String,String> temp =null;
	private Map<String,String> alertContent = null;
	private String productTranReferenceNumber = null;
	private String sAlertMonitoring = null;
	
	public RestAPISteps(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Alert Trigger");
		alertExcelData = excel.getExcelDataIntoHashMap();		
	}

	@Then("I verify the status code is (\\d+)")
	public void verify_status_code(int statusCode){
		response.then().statusCode(statusCode);
	}

	@Given("^I link the package with cif_guid for \"(.*)\"$")
	public void I_link_the_package_with_cif_guid_for(String sAlertTriggerName){
		this.sAlertTriggerName = sAlertTriggerName;
		temp = CSAutomationHooks.alertJSONData.get(this.sAlertTriggerName);
		alertContent = alertExcelData.get(sAlertTriggerName);
		Log.info("ALERT_PACKAGE_CODE = " + temp.get("alertPackageCode"));
		Log.info("CIF_GUID = " + alertContent.get("CIF_GUID"));
		sAlertPackageCode = temp.get("alertPackageCode");
		Log.info("sAlertPackageCode from JSON= " + sAlertPackageCode);
		DatabaseConnection.getInstance().executeUpdateQuery(String.format(DBQueries.ALERT_PACKAGE_UPDATE_CLOSE));
		DatabaseConnection.getInstance().executeUpdateQuery(String.format(DBQueries.ALERT_PACKAGE_UPDATE_OPEN, sAlertPackageCode));
		DatabaseConnection.getInstance().executeDBPreparedQuery(DBQueries.ALERT_PACKAGE_LINKAGE, alertContent.get("CIF_GUID"));
		Log.info("sAlertPackageCode from DB= " + DatabaseConnection.getInstance().getResultSetToList().get(0).get("ALERT_PACKAGE_CODE"));
		if(temp.get("alertPackageCode").equals(DatabaseConnection.getInstance().getResultSetToList().get(0).get("ALERT_PACKAGE_CODE"))){
			Log.info("linkage found");
		}else{
			Log.info("linkage not found");
			DatabaseConnection.getInstance().executeUpdateQuery(String.format(DBQueries.ALERT_PACKAGE_LINKAGE_UPDATE, sAlertPackageCode, alertContent.get("CIF_GUID")));
			Log.info("linkage successful");
		}
		
	} 

	@Given("^I trigger the alert$")
	public void I_trigger_the_alert() throws Exception{
//		String outString = FileUtils.readFileToString(new File(AutomationHooks.RESOURCES_PATH+"/TestData/AlertsRequest.json"), "UTF-8");
		String outString = createAlertRequest();
		request = given().header("Content-Type", "application/json").body(outString);
		System.out.println("END PIT = " + FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT"));
		response = request.when().post(FileReaderManager.getInstance().getConfigReader().getProperty("END_POINT"));
	}

	private String createAlertRequest() throws Exception {
		Map<String,Object> data = new LinkedHashMap<>();
		data.put("bankEntityId", FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME());
		if(alertContent.get("Product").equals("Common Services")){
			data.put("product", "COMMON");
		}else if(alertContent.get("Product").equals("Payments")){
			data.put("product", "PAYMNT");	
		}
		data.put("alertEvent", alertContent.get("Event"));
		productTranReferenceNumber = KeyGenerator.getrandomAlphanumericString(20);
		data.put("productTranReferenceNumber", productTranReferenceNumber);
		data.put("cifGUID", alertContent.get("CIF_GUID"));		
		data.put("bankMakerId", alertContent.get("BankMakerId"));
		data.put("custMakerID", alertContent.get("CustMakerID"));
		data.put("counterPartyMobileNumber", alertContent.get("CounterPartyMobileNumber"));
		data.put("counterPartyEmailId", alertContent.get("CounterPartyEmailId"));
		data.put("customerAdviceTemplate", alertContent.get("CustomerAdviceTemplate"));
		//data.put("counterpartyAdviceTemplate", alertContent.get("CounterpartyAdviceTemplate"));
		data.put("preResolvedAlertMesssageTemplateCode", alertContent.get("PreResolvedAlertMesssageTemplateCode"));
		data.put("preResolvedDestinationEmailIds", alertContent.get("PreResolvedDestinationEmailIds"));
		data.put("preResolvedDestinationMobileNos", alertContent.get("PreResolvedDestinationMobileNos"));
		data.put("guid", alertContent.get("Guid"));
		List<Map<String,String>> list = new ArrayList<>();
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Event Attributes");
		alertExcelData = excel.getExcelDataIntoHashMap();	
		alertExcelData.forEach((key,value)-> {Log.info("Key : " + key + " Value : " + value);
		Map<String,String> map = new LinkedHashMap<>();
		if(!value.get(this.sAlertTriggerName).equalsIgnoreCase("")){
			String[] strings = value.get(this.sAlertTriggerName).split("=", 2);
			map.put("sequence", key);
			map.put("attributeId", strings[0]);
			if(strings[1].equalsIgnoreCase("CurrentDate")){
				map.put("attributeValue", KeyGenerator.getDate("CurrentDate","yyyy-MM-dd"));
			}else{
				map.put("attributeValue", strings[1]);
			}
		}else{
			return;
		}
		list.add(map);
		});
		data.put("attributes", list);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String outString = gson.toJson(data);
		new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/AlertTrigger/").mkdirs();
		File destFile = new File(AutomationHooks.RESOURCES_PATH+"/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+"/"+new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+"/AlertTrigger/"+alertContent.get("Product")+"_"+alertContent.get("Event")+"_"+productTranReferenceNumber+".json");
		FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		return outString;
	}
	
	@Then("^I search for the Alert triggered for \"([^\"]*)\"$")
	public void i_search_for_the_Alert_triggered_for(String sAlertMonitoring) throws Exception {
		this.sAlertMonitoring = sAlertMonitoring;
		
		temp = CSAutomationHooks.alertJSONData.get(this.sAlertMonitoring);
		selectElementFromList("MoniProduct",alertContent.get("Product"));
		enterText("Transaction Reference Number",productTranReferenceNumber);
		clickElement("Search");
		Thread.sleep(4000);
		clickElement("SMSIcon",productTranReferenceNumber);
		Thread.sleep(4000);
		//String str2 = getText("SMSHeader");
		assertTrue(getText("SMSHeader").equals("SMS Notification Details"));
		clickElement("Close");
		
		clickElement("EmailIcon",productTranReferenceNumber);
		clickElement("Close");
		
		
		
		
	}
}

package org.igtb.cs.steps;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.utilities.DatabaseConnection;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.igtb.itaf.ipsh.utilities.Log;
import com.google.gson.Gson;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class CSAutomationHooks extends AutomationHooks{

	TestContext testContext;

	public static Map<String,String> scenarioTestData;
	public static Map<String,Map<String,String>> alertJSONData = null;
	public static Map<String,Map<String,String>> chargeJSONData = null;
	public static Map<String,Map<String,String>> billpayJSONData = null;
	
	private Gson gson = new Gson();	
	private ExcelUtils excel;

	public CSAutomationHooks(TestContext context) {
		super(context);
		testContext = context;
	}

	@SuppressWarnings("unchecked")
	@Before
	public void BeforeScenario(Scenario scenario) throws Exception{
		super.BeforeScenario(scenario);	
		excel = new ExcelUtils();
		DatabaseConnection.getInstance().getConnection();
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", FileReaderManager.getInstance().getConfigReader().getSHEET_NAME());
		scenarioTestData = excel.getExcelDataIntoHashMap().get(AutomationHooks.scenario_id);
		Log.info("scenarioTestData = " + scenarioTestData);
		alertJSONData = gson.fromJson(new FileReader(new File(AutomationHooks.RESOURCES_PATH+"/TestData/Alerts.json")), Map.class);
		chargeJSONData = gson.fromJson(new FileReader(new File(AutomationHooks.RESOURCES_PATH+"/TestData/Charges.json")), Map.class);
		billpayJSONData = gson.fromJson(new FileReader(new File(AutomationHooks.RESOURCES_PATH+"/TestData/BillPayment.json")), Map.class);
		Log.info("AlertJSONData = " + CSAutomationHooks.alertJSONData);
		Log.info("chargeJSONData = " + CSAutomationHooks.chargeJSONData);
	}	 

	@After
	public void AfterScenario(Scenario scenario) throws Exception {
		super.AfterScenario(scenario);
		DatabaseConnection.getInstance().closeConnection();
		if(alertJSONData != null){
			String outString = gson.toJson(alertJSONData);
			File destFile = new File(AutomationHooks.RESOURCES_PATH+"/TestData/Alerts.json");
			FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		}
		
		if(chargeJSONData != null){
			String outString = gson.toJson(chargeJSONData);
			File destFile = new File(AutomationHooks.RESOURCES_PATH+"/TestData/Charges.json");
			FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		}
		
		if(billpayJSONData != null){
			String outString = gson.toJson(billpayJSONData);
			File destFile = new File(AutomationHooks.RESOURCES_PATH+"/TestData/BillPayment.json");
			FileUtils.writeStringToFile(destFile, outString, "UTF-8");
		}
	}

	public static String getOutputFileName(String inputString) {
		String outputString = ""; 
		String[] strings = null;
		String[] newStrings = null;
		Date date = new Date();
		if(inputString.contains("(")){
			strings = StringUtils.substringsBetween(inputString, "(",")");
			newStrings = new String[strings.length];
			int i=0;
			for (String string : strings) {
				newStrings[i] = new SimpleDateFormat(string).format(date);
				i++;
			}
		}
		if(inputString.contains("#")){
			for(int j=0; j<inputString.length();j++){
				if(inputString.charAt(j) == '#'){
					outputString = outputString+KeyGenerator.getRandomInteger(1);
				}else{
					outputString = outputString+inputString.charAt(j);
				}
			}
		}else{
			outputString = inputString;
		}
		outputString = StringUtils.replaceEach(outputString, strings, newStrings).replaceAll("\\(", "").replaceAll("\\)", "");
		Log.info("outputString  = " + outputString);		
		return outputString;
	}
}

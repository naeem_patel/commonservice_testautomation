package org.igtb.cs.steps;

import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.KeyGenerator;
import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TaxSetSteps extends BaseFunctions {
	TestContext testContext;
	private ExcelUtils excel = new ExcelUtils();
	private Map<String,Map<String,String>> TaxSetExcelData = null;
	private String sTaxSetName = null; 
	private String sTaxSetCode = null; 
	private String sTaxSetDes = null;
	private Map<String,String> temp =null;
	
	public TaxSetSteps(TestContext context) throws Exception {
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls","Tax Set");
		
		TaxSetExcelData = excel.getExcelDataIntoHashMap();
		
	}

	@When("^I add Tax Set for \"(.*)\"$") 
	public void I_add_Tax_Set(String sTaxSetName) throws Exception
	{
		this.sTaxSetName = sTaxSetName;
		
		Map<String,String> TaxSetContent = TaxSetExcelData.get(sTaxSetName);
		temp = CSAutomationHooks.chargeJSONData.get(this.sTaxSetName);
		
		 sTaxSetCode = "AUTO"+KeyGenerator.getDate("CurrentDate", "ddMMyyyyHHmmss");
		 sTaxSetDes = sTaxSetName+sTaxSetCode;
		clickElement("Add New Tax Set");
		enterText("Tax Set Code",sTaxSetCode);
	      enterText("Tax Set Des",sTaxSetDes);
	      clickElement("Add New Tax Code Button");
	      
	      enterText("Tax Code",TaxSetContent.get("Tax Code"));
	      enterText("Tax Code Desc",TaxSetContent.get("Tax Code Desc"));
	      enterText("Rate",TaxSetContent.get("Rate"));
	      enterText("Tax Credit GL Acc",TaxSetContent.get("Tax Credit GL AC"));
	      Thread.sleep(3000);
	  	clickElement("NG2-AUTO-COMPLETE1");
	      temp.put("taxSetCode", sTaxSetCode);
	      

	  	if(isElementEnabled("Submit")){
	  		clickElement("Submit");
	  	}else{
	  		Assert.assertTrue("Error while adding Charge Code",false);
	  	}
	      
	      
	}
	@Then("^I approve Tax Set$")
	public void I_approve_Charge_Package() throws Exception 
	{
		clickElement("Tax Set Code Filter");
		enterText("Tax Set Code Filter TextBox",sTaxSetCode+"\n");	
		
		clickElement("Tax Set Code CheckBox",sTaxSetCode);
		clickElement("Approve1");
		enterText("Remarks","Approved"+sTaxSetCode);
		clickElement("APPROVE Remarks");
		
	}
	@And("^I verify that Tax Set is \"(.*)\" successfully$")
	public void I_verify_that_Charge_Package_status(String sStatus) throws Exception{
		
		Thread.sleep(5000);
		clickElement("Tax Set Code Filter");
		    enterText("Tax Set Code Filter TextBox",sTaxSetCode+"\n");	
		Assert.assertTrue("Tax Set Code Status"+sStatus+" is not found for "+sTaxSetCode, getText("Tax Set Code Status",sTaxSetCode).equals(sStatus));
	temp.put("taxSetCodeStatus", sStatus);
		CSAutomationHooks.chargeJSONData.put(sTaxSetName, temp);
	
	}
}


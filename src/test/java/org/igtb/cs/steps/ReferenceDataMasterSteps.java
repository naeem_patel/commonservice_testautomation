package org.igtb.cs.steps;
import java.util.Map;

import org.igtb.itaf.ipsh.cucumber.TestContext;
import org.igtb.itaf.ipsh.managers.FileReaderManager;
import org.igtb.itaf.ipsh.steps.AutomationHooks;
import org.igtb.itaf.ipsh.steps.BaseFunctions;
import org.igtb.itaf.ipsh.utilities.ExcelUtils;
import org.igtb.itaf.ipsh.utilities.Log;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
public class ReferenceDataMasterSteps extends BaseFunctions {
	
		TestContext testContext;
		private ExcelUtils excel = new ExcelUtils();
		private Map<String,Map<String,String>> alertExcelData = null;
		private String sAlertMessageTemplateName = null; 
		private String sAlertMessageTemplateCode = null; 
		private String sAlertMessageDescription = null;
		private Map<String,String> temp =null;
	
		public ReferenceDataMasterSteps(TestContext context) throws Exception{
		super(context);
		testContext = context;
		excel.setExcelFile(AutomationHooks.RESOURCES_PATH+"/TestData/"+FileReaderManager.getInstance().getConfigReader().getCLIENT_NAME()+".xls", "Reference Data");
		alertExcelData = excel.getExcelDataIntoHashMap();
	}

}


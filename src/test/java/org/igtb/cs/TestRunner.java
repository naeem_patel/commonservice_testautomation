package org.igtb.cs;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features",	
		glue ={"classpath:"},
		strict = true,
		monochrome = true,
		//tags ={"@Smoke1,@Smoke2,@Smoke3,@Smoke4"},
				//tags ={"@GTBIPSH-CS321"},
				 tags ={"@GTBIPSH-CS312,@GTBIPSH-CS313,@GTBIPSH-CS314,@GTBIPSH-CS318,@GTBIPSH-CS320,@GTBIPSH-CS321"},
		plugin={"pretty","html:target/cucumber-parallel","json:target/cucumber-parallel/cucumber.json"}
		)
public class TestRunner {
}
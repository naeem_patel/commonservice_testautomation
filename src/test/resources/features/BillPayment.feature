@BillPayment
Feature: Bill Payment Feature

@GTBIPSH-CS312
Scenario Outline: New Bill Category Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Bill Category Screen
	When I add New Bill Category "<Biller Category>"
	And I verify that Bill Category is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Bill Category
	And I verify that Bill Category is "Approved" successfully
	Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3 |
		
@GTBIPSH-CS313
Scenario Outline: New Bill Maintenance Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Bill Maintenance Screen
	When I add New Biller "<Biller Category>"
	And I verify that Biller is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Biller
	And I verify that Biller is "Approved" successfully
	Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3|
		
@GTBIPSH-CS314
Scenario Outline: New Bill Service Configuration Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Biller Service Configuration Screen
	When I add New Biller Service Configuration "<Biller Category>"
	And I verify that Biller Service Configuration is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Biller Service Configuration
	And I verify that Biller Service Configuration is "Approved" successfully
	Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3|
		
		
@GTBIPSH-CS315
Scenario Outline: Biller List Processing	
		Given I trigger the Biller List Processing "<Biller Category>"
		Then I verify the status response message for Biller List Processing
		Examples:
		| Biller Category |
		|CS_BILLERLIST_VAL_1|
		|CS_BILLERLIST_VAL_2|
		|CS_BILLERLIST_VAL_3|
		|CS_BILLERLIST_VAL_4|
		|CS_BILLERLIST_VAL_5|
		|CS_BILLERLIST_VAL_6|
		|CS_BILLERLIST_VAL_7|
		

@GTBIPSH-CS316
Scenario Outline: Biller Parameter Field Processing	
		Given I trigger the Parameter Field Processing "<Biller Category>"
		Then I verify the status response message for Biller Parameter Processing
		
		Examples:
		| Biller Category |
		| CS_BILL_CATEGORY|
		
@GTBIPSH-CS317
Scenario Outline: Biller Category Service Type Processing	
		Given I trigger the Biller Category Service Type Processing "<Biller Category>"
		Then I verify the status response message for Biller Category Service Type Processing
		Examples:
		| Biller Category |
		| CS_BILL_CATEGORY|
		

@GTBIPSH-CS318
Scenario Outline: Payer Registration Processing
		Given I am on Login Page
		And I switch to "Alice" User
		And I am on Payer Registration Screen
		When I trigger the Payer Registration Processing "<Biller Category>"
		Then I verify the status response message for Payer Registration
		
		
		Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3|
		
		
@GTBIPSH-CS319
Scenario Outline: Bill Presentment Processing	
		Given I trigger Bill Presentment for processing "<Biller Category>"
		And I verify the processing status
		
		Examples:
		| Biller Category |
		| CS_BILL_CATEGORY|
		
	@GTBIPSH-CS320
Scenario Outline: Online Bill Fetch Processing	
		Given I trigger Online Bill Fetch for processing "<Biller Category>"
		And I verify the the status response message for Online Bill Fetch is "SUCCESS"
		
		Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3|	
		
		
		@GTBIPSH-CS321
Scenario Outline: Bill Payment Processing	
		Given I trigger Bill Payment for processing "<Biller Category>"
		And I verify the the status response message for Bill Payment is "SUCCESS"
		
			Examples:
		| Biller Category |
		| CS_BILL_CATEGORY3|	
		
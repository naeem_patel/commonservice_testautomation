@Charges
Feature: Charges Feauter
 

  
  	@GTBIPSH-CS212
	Scenario Outline: Charges Creation Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Charges Page
	When I add Charge Code for "<Charge Name>"
	And I verify that Charge Code is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Charge Code
	And I verify that Charge Code is "Approved" successfully
	Examples:
		| Charge Name  |
		| CS_CHG_COD|
		| CS_CHG_COD1|
		| CS_CHG_COD2|	
		  
		  
	

		@GTBIPSH-CS213	
	Scenario Outline: Charge Pacakge Creation Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Charge Package Page
	When I add Charge Package for "<Charge Package Name>"
	And I verify that Charge Package is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Charge Package 
	And I verify that Charge Package is "Approved" successfully
	Examples:
		| Charge Package Name  |
		| CS_CHG_GOLD |

	@GTBIPSH-CS214	
	Scenario Outline: Tax Set Creation Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Tax Set Page
	When I add Tax Set for "<Tax Set Name>"
	And I verify that Tax Set is "Submitted" successfully
	And I switch to "Bob" User
	Then I approve Tax Set 
	And I verify that Tax Set is "Approved" successfully
	Examples:
		| Tax Set Name  |
		| CS_CHG_TAX |
		
		
@GTBIPSH-CS215
 	Scenario Outline: Charge Trigger and Monitoring Test
    Given I link the package with cif_guid for charge "<Charge Trigger Name>"
	And I trigger the Charge Processing
	Then I verify the return charge event status code is 200
	Then I verify billing status and charge amount total for package "<Charge Trigger Name>"
	And I process EOD batch for "THAILAND" entity
	And I verify billing status of deffered codes for package "<Charge Trigger Name>"
	
	Examples:
	|Charge Trigger Name|
	| CS_CHG_COD|
				
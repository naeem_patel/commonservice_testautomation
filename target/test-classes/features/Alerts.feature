@Alerts
Feature: Alerts Feature

@GTBIPSH-CS2 @Smoke1
	Scenario Outline: Message Creation Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	When I add Message Template for "<Message Template Name>"
	And I verify that Message Template is "Submitted" successfully
	And I switch to "Bob" User
	And I am on Alerts Page
	Then I approve Message Template
	And I verify that Message Template is "Approved" successfully
	Examples:
		| Message Template Name  |
		| CS_CHG_POST1 |
		

@GTBIPSH-CS10 @Smoke2
	Scenario Outline: Alert Subscription Test With Hold Time
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	When I add New Alert Subscription for "<Alert Subscription Name>" with hold time
	And I verify that Alert Subscription is "Submitted" successfully
	And I switch to "Bob" User
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	Then I approve Alert Subscription
	And I verify that Alert Subscription is "Approved" successfully
	Examples:
		| Alert Subscription Name  |
		| CS_CHG_POST1 |

@GTBIPSH-CS4 @Smoke3
	Scenario Outline: Alert Package Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	And I am on Alerts Package Page
	When I add New Alert Package for "<Alert Package Name>"
	And I verify that Alert Package is "Submitted" successfully
	And I switch to "Bob" User
	And I am on Alerts Page
	And I am on Alerts Package Page
	Then I approve Alert Package
	And I verify that Alert Package is "Approved" successfully
	Examples:
		| Alert Package Name  |
		| Package1 |


@GTBIPSH-CS5 @Smoke4
 	Scenario Outline: Alert Trigger and Monitoring Test
    Given I link the package with cif_guid for "<Alert Trigger Name>"
	And I trigger the alert
	Then I verify the status code is 200
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	And I am on Alert Monitoring Page
	Then I search for the Alert triggered for "<Alert Trigger Name1>"
	
	Examples:
	|Alert Trigger Name||Alert Trigger Name1|
	|CS_CHG_POST1||CS_CHG_POST1|


	@GTBIPSH-CS1	
	Scenario: Login Test for ARX
	Given I am on ARX Login Page
	When I login with "Maker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	Then I logout from application for ARX

			
		
	

		
	@GTBIPSH-CS3
	Scenario Outline: Alert Subscription Test
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	When I add New Alert Subscription for "<Alert Subscription Name>"
	And I verify that Alert Subscription is "Submitted" successfully
	And I switch to "Bob" User
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	Then I approve Alert Subscription
	And I verify that Alert Subscription is "Approved" successfully
	Examples:
		| Alert Subscription Name  |
		| CS_CHG_POST1 |

			
	
		
	
	@GTBIPSH-CS6
	Scenario Outline: Message Creation Test Using ARX
	Given I am on ARX Login Page
	When I login with "Maker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	When I add Message Template for "<Message Template Name>"
	And I verify that Message Template is "Submitted" successfully
	Then I logout from application for ARX
	Given I am on ARX Login Page
	When I login with "Checker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	Then I approve Message Template
	And I verify that Message Template is "Approved" successfully
	Then I logout from application for ARX
	Examples:
		| Message Template Name  |
		| CS_CHG_POST |
		
	@GTBIPSH-CS7
	Scenario Outline: Alert Subscription Test Using ARX
	Given I am on ARX Login Page
	When I login with "Maker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	When I add New Alert Subscription for "<Alert Subscription Name>"
	And I verify that Alert Subscription is "Submitted" successfully
	Given I am on ARX Login Page
	When I login with "Checker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	And I am on Alerts Subscription Page
	Then I approve Alert Subscription
	And I verify that Alert Subscription is "Approved" successfully
	Examples:
		| Alert Subscription Name  |
		| CS_CHG_POST |
		
	@GTBIPSH-CS8
	Scenario Outline: Alert Package Test Using ARX
	Given I am on ARX Login Page
	When I login with "Maker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	And I am on Alerts Package Page
	When I add New Alert Package for "<Alert Package Name>"
	And I verify that Alert Package is "Submitted" successfully
	Given I am on ARX Login Page
	When I login with "Checker" for ARX
	And I select "CommonServicesQC" Web Application for ARX
	And I am on Alerts Page
	And I am on Alerts Package Page
	Then I approve Alert Package
	And I verify that Alert Package is "Approved" successfully
	Examples:
		| Alert Package Name  |
		| Package1 |	
		
		
	
	
	
	
@AlertTrigger
	Feature: Alerts Trigger Feature	
		
	@GTBIPSH-CS9 
 	Scenario Outline: Alert Trigger and Monitoring Test
    Given I link the package with cif_guid for "<Alert Trigger Name>"
	And I trigger the alert
	Then I verify the status code is 200
	Given I am on Login Page
	And I switch to "Alice" User
	And I am on Alerts Page
	And I am on Alert Monitoring Page
	Then I search for the Alert triggered for "<Alert Trigger Name1>"
	
	Examples:
	|Alert Trigger Name||Alert Trigger Name1|
	|CS_CHG_POST||CS_CHG_POST|
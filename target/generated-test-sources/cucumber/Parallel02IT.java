import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"C:/Users/naeem.patel/CSAuto/CommonServices-Test_Automation/src/test/resources/features/BillPayment.feature:54"},
        plugin = {"json:C:/Users/naeem.patel/CSAuto/CommonServices-Test_Automation/target/cucumber-parallel/2.json", "html:C:/Users/naeem.patel/CSAuto/CommonServices-Test_Automation/target/cucumber-parallel/2", "rerun:C:/Users/naeem.patel/CSAuto/CommonServices-Test_Automation/target/cucumber-parallel/2.txt"},
        monochrome = true,
        glue = {"org.igtb.cs.steps"})
public class Parallel02IT {
}
